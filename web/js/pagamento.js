axios.get('http://localhost:3000/pagamentos').then(resp => {
		var tbody = document.getElementById('conteudo-pagamentos');
		resp.data.forEach(function (value) {
			var tr = document.createElement('tr');
			var id = value.id;
			var td_tipo = document.createElement('td');
			var td_nome = document.createElement('td');
			var td_taxa = document.createElement('td');
			var td_qtd_vezes = document.createElement('td');
			var td_editar = document.createElement('td');

			td_tipo.innerHTML = value.tipo;
			td_nome.innerHTML = value.nome;
			td_taxa.innerHTML = value.taxa;
			td_qtd_vezes.innerHTML = value.qtd_vezes;

			td_editar.innerHTML =
				"<center>" +
				"<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
				"<i class='fas fa-edit' ></i>" +
				"</button>" +
				"</center>";

			tr.appendChild(td_tipo);
			tr.appendChild(td_nome);
			tr.appendChild(td_taxa);
			tr.appendChild(td_qtd_vezes);
			tr.appendChild(td_editar);

			tbody.appendChild(tr);
		});

		$(document).ready(function () {
			$('#dataTable').DataTable({});
		});
	});


	function addPagamento() {
		$('#AddPag').modal('show');
	};

	async function cadastrar_pagamento() {
		if ($('#nome_pag').val() != "" && $('#tipo_pag').val() != ""
			&& $('#taxa_pag').val() != "" && $('#qtd_vezes_pag').val() != "") {

			var nome = $('#nome_pag').val();
			var tipo = $('#tipo_pag').val();
			var taxa = $('#taxa_pag').val();
			var qtd_vezes = $('#qtd_vezes_pag').val();

			await axios({
				//tras todos as plenarias no option select
				method: 'POST',
				url: 'http://localhost:3000/pagamentosCreate',
				data: {
					nome,
					tipo,
					taxa,
					qtd_vezes
				},
				headers: {}
			})
				.then(function (response) {
					$('#AddPag').modal('hide');
					$('#confirmacaoCadastro').modal('show');
					setTimeout(
						function () {
							location.reload();
						}, 2000
					);
				})
				.catch(function (response) {
					console.log(error);
					$('#AddPag').modal('hide');
					$('#negacaoCadastro').modal('show');
					setTimeout(
						function () {
							location.reload();
						}, 2000
					);
				});
		} else {
			alert("Campos incompletos");
		}
	};

	function edit(id) {
		$('#EditPag').modal('show');
		axios.get('http://localhost:3000/pagamentosGetEdit?id=' + id).then(resp => {
			resp.data.forEach(function (value) {
				$('#id_pag_edit').val(value.id);
				$('#nome_pag_edit').val(value.nome);
				$('#tipo_pag_edit').val(value.tipo);
				$('#taxa_pag_edit').val(value.taxa);
				$('#qtd_vezes_pag_edit').val(value.qtd_vezes);
			});
		});
	}

	function editar_pagamento() {
		if ($('#nome_pag_edit').val() != "" && $('#tipo_pag_edit').val() != ""
			&& $('#taxa_pag_edit').val() != "" && $('#qtd_vezes_pag_edit').val() != "") {
			
			var id = $('#id_pag_edit').val();
			var nome = $('#nome_pag_edit').val();
			var tipo = $('#tipo_pag_edit').val();
			var taxa = $('#taxa_pag_edit').val();
			var qtd_vezes = $('#qtd_vezes_pag_edit').val();

			axios.put('http://localhost:3000/pagamentosEdit?id=' + id, {
				nome,
				tipo,
				taxa,
				qtd_vezes
			})
				.then(response => {
					$('#EditPag').modal('hide');
					$('#confirmacaoEditar').modal('show');
					setTimeout(
						function () {
							location.reload();
						}, 2000
					);
				})
				.catch(error => {
					console.log(error);
				});
		} else {
			alert("Campos incompletos");
		}
	}
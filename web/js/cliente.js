$(document).ready(function () {
    $("#buscaCep").click(function () {

        //Nova variável "cep" somente com dígitos.
        var cep = $("#cep").val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#endereco").val(dados.logradouro);
                        $("#endereco").css("background", "#eee");
                        $("#bairro").val(dados.bairro);
                        $("#bairro").css("background", "#eee");
                        $("#cidade").val(dados.localidade);
                        $("#cidade").css("background", "#eee");
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                alert("Formato de CEP inválido.");
            }
        } //end if.
    });
});

axios.get('http://localhost:3000/clientes').then(resp => {
    var tbody = document.getElementById('conteudo-clientes');
    resp.data.forEach(function (value) {
        var tr = document.createElement('tr');
        var id = value.id;
        var td_nome = document.createElement('td');
        var td_nasc = document.createElement('td');
        var td_cpf = document.createElement('td');
        var td_rg = document.createElement('td');
        var td_endereco = document.createElement('td');
        var td_tel = document.createElement('td');
        var td_limite = document.createElement('td');
        var td_editar = document.createElement('td');

        td_nome.innerHTML = value.nome;
        td_nasc.innerHTML = value.data_nasc;
        td_cpf.innerHTML = value.cpf;
        td_rg.innerHTML = value.rg;
        td_endereco.innerHTML = value.endereco + ", "
            + value.numero + " - "
            + value.bairro + " "
            + value.cidade;
        td_tel.innerHTML = value.telefone;
        td_limite.innerHTML = value.limite_venda;

        td_editar.innerHTML =
            "<center>" +
            "<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
            "<i class='fas fa-edit' ></i>" +
            "</button>" +
            "</center>";

        tr.appendChild(td_nome);
        tr.appendChild(td_nasc);
        tr.appendChild(td_cpf);
        tr.appendChild(td_rg);
        tr.appendChild(td_endereco);
        tr.appendChild(td_tel);
        tr.appendChild(td_limite);
        tr.appendChild(td_editar);

        tbody.appendChild(tr);
    });

    $(document).ready(function () {
        $('#dataTable').DataTable({});
    });
});


function addCliente() {
    $('#AddCli').modal('show');
};

async function cadastrar_cliente() {
    if ($('#nome').val() != "" &&
        $('#data_nasc').val() != "" && $('#cpf').val() != "" &&
        $('#tel').val() != "" && $('#limite_venda').val() != "" &&
        $('#cep').val() != "" && $('#endereco').val() != "" &&
        $('#numero').val() != "" && $('#bairro').val() != "" &&
        $('#cidade').val()) {

        //DENTRO DO IF DE VERIFICAÇÃO
        var nome = $('#nome').val();
        var data_nasc = $('#data_nasc').val();
        var cpf = $('#cpf').val();
        var rg = $('#rg').val();
        var limite_venda = $('#limite_venda').val();
        var email = $('#email').val();
        var telefone = $('#tel').val();
        var cep = $('#cep').val();
        var endereco = $('#endereco').val();
        var numero = $('#numero').val();
        var bairro = $('#bairro').val();
        var cidade = $('#cidade').val();
        var observacao = $('#observacao').val();

        await axios({
            //tras todos as plenarias no option select
            method: 'POST',
            url: 'http://localhost:3000/clientesCreate',
            data: {
                nome,
                data_nasc,
                cpf,
                rg,
                email,
                cep,
                endereco,
                numero,
                bairro,
                cidade,
                telefone,
                observacao,
                limite_venda
            },
            headers: {}
        })
            .then(function (response) {
                $('#AddCli').modal('hide');
                $('#confirmacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(function (response) {
                console.log(error);
                $('#AddCli').modal('hide');
                $('#negacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            });
    } else {
        alert("Campos incompletos");
    }
};

function edit(id) {
    $('#EditCli').modal('show');
    axios.get('http://localhost:3000/clientesGetEdit?id=' + id).then(resp => {
        resp.data.forEach(function (value) {
            $('#id_edit').val(value.id);
            $('#nome_edit').val(value.nome);
            $('#data_nasc_edit').val(value.data_nasc);
            $('#cpf_edit').val(value.cpf);
            $('#rg_edit').val(value.rg);
            $('#limite_venda_edit').val(value.limite_venda);
            $('#email_edit').val(value.email);
            $('#tel_edit').val(value.telefone);
            $('#cep_edit').val(value.cep);
            $('#endereco_edit').val(value.endereco);
            $('#numero_edit').val(value.numero);
            $('#bairro_edit').val(value.bairro);
            $('#cidade_edit').val(value.cidade);
            $('#observacao_edit').val(value.observacao);
        });
    });
}

function mascara(i) {
    var v = i.value;

    if (isNaN(v[v.length - 1])) { // impede entrar outro caractere que não seja número
        i.value = v.substring(0, v.length - 1);
        return;
    }

    i.setAttribute("maxlength", "14");
    if (v.length == 3 || v.length == 7) i.value += ".";
    if (v.length == 11) i.value += "-";
}

function editar_cliente() {
    if ($('#nome_edit').val() != "" &&
        $('#data_nasc_edit').val() != "" && $('#cpf_edit').val() != "" &&
        $('#tel_edit').val() != "" && $('#limite_venda_edit').val() != "" &&
        $('#cep_edit').val() != "" && $('#endereco_edit').val() != "" &&
        $('#numero_edit').val() != "" && $('#bairro_edit').val() != "" &&
        $('#cidade_edit').val()) {

        var id = $('#id_edit').val();
        var nome = $('#nome_edit').val();
        var data_nasc = $('#data_nasc_edit').val();
        var cpf = $('#cpf_edit').val();
        var rg = $('#rg_edit').val();
        var limite_venda = $('#limite_venda_edit').val();
        var email = $('#email_edit').val();
        var telefone = $('#tel_edit').val();
        var cep = $('#cep_edit').val();
        var endereco = $('#endereco_edit').val();
        var numero = $('#numero_edit').val();
        var bairro = $('#bairro_edit').val();
        var cidade = $('#cidade_edit').val();
        var observacao = $('#observacao_edit').val();

        axios.put('http://localhost:3000/clientesEdit?id=' + id, {
            nome,
            data_nasc,
            cpf,
            rg,
            email,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            telefone,
            observacao,
            limite_venda
        })
            .then(response => {
                console.log(response);
                $('#EditCli').modal('hide');
                $('#confirmacaoEditar').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(error => {
                console.log(error);
            });
    } else {
        alert("Campos incompletos");
    }
}
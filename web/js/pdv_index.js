function findGetParameter(parameterName) {
    var result = null,
        tmp = [];
    location.search
        .substr(1)
        .split("&")
        .forEach(function (item) {
            tmp = item.split("=");
            if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
        });
    return result;
}
var id_caixa = findGetParameter('id_caixa');

axios.get('http://localhost:3000/pdvStatusCaixa?id_caixa='+id_caixa).then(resp => {
    if (resp.data.length == 0) {
        alert("Caixa Inválido!");
        window.location.href = 'abrir_caixa.html';
    }
});

$(document).ready(function () {
    $('#cli_pdv').selectize({
        create: false,
        sortField: 'text'
    });

    axios.get('http://localhost:3000/empresasGetEdit').then(resp => {
        resp.data.forEach(function (value) {
            document.getElementById('logo-horiz').setAttribute('src', value.logo_principal_g);
        });
    });

    var selectize_cli = document.getElementById("cli_pdv");
    axios.get('http://localhost:3000/clientes').then(resp => {
        resp.data.forEach(function (value) {
            selectize_cli.selectize.addOption({ value: value.id, text: value.nome });
        });
    });
});

var tbody = document.getElementById('conteudo-pdv');

var inicio = 1;

var valor_total_venda = 0;

var prods = [];
var qtds = [];
var precos = [];

var formas_pag = [];
var valores_pag = [];
var formas_pag_exec_qtd = 0;

$('#valorTotalVenda').html(new Intl.NumberFormat('pt-BR', {
    style: 'currency',
    currency: 'BRL'
}).format(valor_total_venda));

function voltar_sistema() {
    window.location = "../index.html";
}

function verifica_qtd(obj) {
    if (obj.value > parseFloat($('#qtd_prod_max').val())) {
        alert('Quantidade máxima ultrapassada!');
        $('#qtd_prod').val(parseFloat($('#qtd_prod_max').val()));

        var valor_tot = parseFloat($('#qtd_prod_max').val()) * parseFloat($('#valor_un_prod').val());
        $('#valor_tot_prod').val(valor_tot);
    } else {
        var valor_tot = obj.value * parseFloat($('#valor_un_prod').val());
        $('#valor_tot_prod').val(valor_tot);
    }
}

function preencher_campos(obj) {
    axios.get('http://localhost:3000/produtosGetPdv?cod=' + obj.value).then(resp => {
        resp.data.forEach(function (value) {
            var nome_prod = value.nome + " " + value.tamanho + " " + "(" + value.genero + ")" + " - " + value.marca;
            var quantidade = value.quantidade;
            var valor_unitario = value.valor_venda;
            var valor_tot_prod = valor_unitario;

            $('#id_prod').val(value.id);
            $('#nome_prod').val(nome_prod);
            $('#qtd_prod_max').val(quantidade);
            $('#qtd_prod').val(1);
            $('#qtd_prod').removeAttr("readonly");
            $('#qtd_prod').removeClass("readonly");
            $('#valor_un_prod').val(valor_unitario);
            $('#valor_tot_prod').val(valor_tot_prod);
        });
    });
}
var inserir = 0;
var codBarrasInput = document.getElementById('cod_prod');
codBarrasInput.addEventListener('keyup', function (e) {
    var key = e.which || e.keyCode;
    if (key == 13) {
        var id_table = $('#id_prod').val();
        var qtd_table = $('#qtd_prod').val();
        var prod_table = $('#nome_prod').val();
        var preco_table = $('#valor_tot_prod').val();

        if (inserir == 0 && qtd_table != "" && prod_table != "" && preco_table != "") {
            inserir = 1;

        } else if (codBarrasInput.value != "" && inserir == 1) {
            if (qtd_table != "" && prod_table != "" && preco_table != "") {
                if (inicio == 1) {
                    tbody.innerHTML = "";
                    inicio = 0;
                }
                var tr = document.createElement('tr');
                var td_qtd = document.createElement('td');
                var td_prod = document.createElement('td');
                var td_un = document.createElement('td');
                var td_preco = document.createElement('td');
                var td_excluir = document.createElement('td');
                td_excluir.classList.add('td-excluir');

                td_qtd.innerHTML = qtd_table + "x";
                td_prod.innerHTML = prod_table;

                td_un.innerHTML = new Intl.NumberFormat('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }).format(preco_table / qtd_table);

                td_preco.innerHTML = new Intl.NumberFormat('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }).format(preco_table);

                td_excluir.innerHTML =
                    "<center>" +
                    "<button class='btn btn-danger btn-circle' onclick='excluir(" + id_table + "," + preco_table + ",this)' >" +
                    "<i class='fas fa-times' ></i>" +
                    "</button>" +
                    "</center>";

                tr.appendChild(td_qtd);
                tr.appendChild(td_prod);
                tr.appendChild(td_un);
                tr.appendChild(td_preco);
                tr.appendChild(td_excluir);

                tbody.appendChild(tr);
                inserir = 0;

                valor_total_venda += parseFloat(preco_table);
                $('#valorTotalVendaInput').val(valor_total_venda);
                $('#valorTotalVenda').html(new Intl.NumberFormat('pt-BR', {
                    style: 'currency',
                    currency: 'BRL'
                }).format(valor_total_venda));

                prods.push(id_table);
                qtds.push(qtd_table);
                precos.push(preco_table);

                $('#cod_prod').val("");
                $('#qtd_prod').val("");
                document.getElementById('qtd_prod').readOnly = true;
                $('#nome_prod').val("");
                $('#valor_un_prod').val("");
                $('#valor_tot_prod').val("");
            }
        }
    }
});

function adicionar_produto() {
    if ($('#cod_prod').val() != "") {
        var id_table = $('#id_prod').val();
        var qtd_table = $('#qtd_prod').val();
        var prod_table = $('#nome_prod').val();
        var preco_table = $('#valor_tot_prod').val();
        if (qtd_table != "" && prod_table != "" && preco_table != "") {
            if (inicio == 1) {
                tbody.innerHTML = "";
                inicio = 0;
            }
            var tr = document.createElement('tr');
            var td_qtd = document.createElement('td');
            var td_prod = document.createElement('td');
            var td_un = document.createElement('td');
            var td_preco = document.createElement('td');
            var td_excluir = document.createElement('td');
            td_excluir.classList.add('td-excluir');

            td_qtd.innerHTML = qtd_table + "x";
            td_prod.innerHTML = prod_table;

            td_un.innerHTML = new Intl.NumberFormat('pt-BR', {
                style: 'currency',
                currency: 'BRL'
            }).format(preco_table / qtd_table);

            td_preco.innerHTML = new Intl.NumberFormat('pt-BR', {
                style: 'currency',
                currency: 'BRL'
            }).format(preco_table);

            td_excluir.innerHTML =
                "<center>" +
                "<button class='btn btn-danger btn-circle' onclick='excluir(" + id_table + "," + preco_table + ",this)' >" +
                "<i class='fas fa-times' ></i>" +
                "</button>" +
                "</center>";

            tr.appendChild(td_qtd);
            tr.appendChild(td_prod);
            tr.appendChild(td_un);
            tr.appendChild(td_preco);
            tr.appendChild(td_excluir);

            tbody.appendChild(tr);
            inserir = 0;

            valor_total_venda += parseFloat(preco_table);
            $('#valorTotalVendaInput').val(valor_total_venda);
            $('#valorTotalVenda').html(new Intl.NumberFormat('pt-BR', {
                style: 'currency',
                currency: 'BRL'
            }).format(valor_total_venda));

            prods.push(id_table);
            qtds.push(qtd_table);
            precos.push(preco_table);

            $('#cod_prod').val("");
            $('#qtd_prod').val("");
            document.getElementById('qtd_prod').readOnly = true;
            $('#nome_prod').val("");
            $('#valor_un_prod').val("");
            $('#valor_tot_prod').val("");
        }
    }
}

function cancel() {
    var check = confirm("Deseja cancelar a venda?");
    if (check) {
        valor_total_venda = 0;

        inicio = 1;

        prods = [];
        qtds = [];
        precos = [];

        formas_pag = [];
        valores_pag = [];
        formas_pag_exec_qtd = 0;

        tbody.innerHTML = "<tr><td></td><td>Esperando inserção...</td><td></td><td></td><td></td></tr>";

        $('#cod_prod').val("");
        $('#qtd_prod').val("");
        document.getElementById('qtd_prod').readOnly = true;
        $('#nome_prod').val("");
        $('#valor_un_prod').val("");
        $('#valor_tot_prod').val("");

        $('#valorTotalVendaInput').val(valor_total_venda);
        $('#valorTotalVenda').html(new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(valor_total_venda));
    }
}

function excluir(id, preco, obj) {
    //Excluindo a TR toda da lista
    var td = obj.closest('td');
    var tr = td.closest('tr');
    tr.parentNode.removeChild(tr);

    //Verificando se existem produtos iguais para excluir o preço correto
    var prods_aux = [];
    for (var i = 0; i < prods.length; i++) {
        prods_aux.push(prods[i]);
    }

    for (var i = 0; i < prods.length; i++) {
        if (prods_aux.indexOf("" + id) == precos.indexOf("" + preco)) {
            var indice = prods_aux.indexOf("" + id);
        } else {
            prods_aux[prods_aux.indexOf("" + id)] = "x";
        }
    }

    //Removendo produto dos arrays
    var valor_excluir = precos.find(element => precos.indexOf(element) == indice);
    prods.splice(indice, 1);
    qtds.splice(indice, 1);
    precos.splice(indice, 1);

    valor_total_venda -= valor_excluir;
    $('#valorTotalVendaInput').val(valor_total_venda);
    $('#valorTotalVenda').html(new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL'
    }).format(valor_total_venda));
}

function forma_pagamento() {
    if (prods.length > 0) {
        $('#FinVen').modal('hide');
        $('#ForPag').modal('show');

        var valor_total_exbir = $('#valorTotalVendaInput').val();
        $('#valor_total_modal_pay').html(new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(valor_total_exbir));
    } else {
        alert("Não há produtos para finalizar venda.");
    }
}

var icone;
var color;
var tipo;
var selectize_array = [];

var div_pagamentos = document.getElementById('div-pagamentos');
function pagar(p) {
    if (p == "DINHEIRO") {
        tipo = p;
        icone = "fas fa-money-bill-wave";
        color = "1cc88a";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');

    } else if (p == "CARTAO DE DEBITO") {
        tipo = p;
        icone = "far fa-credit-card";
        color = "98a5ff";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');

    } else if (p == "CARTAO DE CREDITO") {
        tipo = p;
        icone = "far fa-credit-card";
        color = "ff92e8";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');

    } else if (p == "CREDIARIO") {
        tipo = p;
        icone = "fas fa-calendar-alt";
        color = "ffe18f";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');

    } else if (p == "BOLETO") {
        tipo = p;
        icone = "fas fa-barcode";
        color = "f7c8c3";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');
    } else if (p == "TRANSFERENCIA") {
        tipo = p;
        icone = "fas fa-donate";
        color = "a3e1ff";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');
    } else if (p == "OUTRO") {
        tipo = p;
        icone = "fas fa-hand-holding-usd";
        color = "ffbe91";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');

    } else if (p == "ISENTO") {
        tipo = p;
        icone = "far fa-handshake";
        color = "5a5858";

        $('#txtFormaPag').html("Dinheiro");

        var selectize_pag = document.getElementById("pag_pdv");
        var select_subcat2 = $('#pag_pdv').selectize();

        axios.get('http://localhost:3000/pagamentosPDV?tipo=' + p).then(resp => {
            for (var i = 0; i < selectize_array.length; i++) {
                select_subcat2[0].selectize.removeOption(selectize_array[i]);
            }

            resp.data.forEach(function (value) {
                selectize_array.push(value.id);
                selectize_pag.selectize.addOption({ value: value.id, text: value.nome });
            });

        });

        $('#ForPag').modal('hide');
        $('#selectPag').modal('show');

    } else {
        alert("Forma de pagamento inválida!");
    }
}

function selecionar() {
    var pag_select = $('#pag_pdv').val();
    if ($('#pag_pdv').val() != "") {

        var adicionar = document.createElement('div');
        axios.get('http://localhost:3000/pagamentosGetEdit?id=' + pag_select).then(resp => {
            resp.data.forEach(function (value) {
                adicionar.innerHTML = "" +
                    "<div id='div_row' class='form-row block-pay'>" +
                    "<div class='col-1 icon-pay'>" +
                    "<i style='font-size:1.5rem; color: #" + color + "' class='" + icone + "'></i>" +
                    "</div>" +
                    "<div class='col-6 text-pay'>" +
                    "<label style='font-size:1.5rem; margin:0'>" + value.nome + "</label>" +
                    "</div>" +
                    "<div id='div_input' class='col-4'>" +
                    "<input type='number' step='0.01' id='valor_pag_" + indice + "' name='valor_pag_" + indice + "' placeholder='Valor'" +
                    "class='form-control input-pay' onchange='calcula_valor_pag(this)' />" +
                    "</div>" +
                    "<div class='col-1 icon-pay-del'>" +
                    "<i onclick='excluir_forma_pag(" + indice + ")' style='font-size:1.5rem; margin-left: 50%;' class='far fa-trash-alt'></i>" +
                    "</div>" +
                    "</div>";
            });
        });


        formas_pag.push(pag_select);
        valores_pag.push(0);
        var indice = (formas_pag.length) - 1;

        $('#selectPag').modal('hide');
        $('#FinVen').modal('show');

        div_pagamentos.appendChild(adicionar);
        formas_pag_exec_qtd++;

    } else {
        alert("Selecione a forma de pagamento!");
    }
}

var desconto = 0;
var valor_pago = 0;
var troco = 0;

function excluir_forma_pag(i) {
    formas_pag[i] = "x";
    valor_pago -= valores_pag[i];
    valores_pag[i] = "0";

    var input = document.getElementById("valor_pag_" + i);
    var col_input = input.closest('#div_input');
    var row = col_input.closest('#div_row');
    row.parentNode.removeChild(row);

    formas_pag_exec_qtd--;

    $('#pagar_modal_pay').html(new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL'
    }).format(valor_pago));

    if (valor_pago > 0) {
        troco = parseFloat(valor_pago) - parseFloat(valor_total_venda);

        // if (troco > 0) {
        $('#troco_modal_pay').html(new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(troco));
        // }
    }
}

function calcula_desconto() {
    valor_total_venda += parseFloat(desconto);
    if (valor_total_venda != $('#desc_pdv').val()) {
        desconto = $('#desc_pdv').val();
        valor_total_venda -= parseFloat(desconto);

        $('#desconto_modal_pay').html(new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(desconto));

        if (valor_pago > 0) {
            troco = parseFloat(valor_pago) - parseFloat(valor_total_venda);

            // if (troco > 0) {
            $('#troco_modal_pay').html(new Intl.NumberFormat('pt-BR', {
                style: 'currency',
                currency: 'BRL'
            }).format(troco));
            // }
        }
    } else {
        alert("Desconto não pode ser igual ao valor! Selecione pagamento Isento.")
    }
}


function calcula_valor_pag(obj) {
    var n = obj.id.split("_");
    valores_pag[n[2]] = obj.value;
    valor_pago = 0;

    for (var i = 0; i < valores_pag.length; i++) {
        if (formas_pag[i] != 0) {
            valor_pago += parseFloat(valores_pag[i]);
        }
    }

    $('#pagar_modal_pay').html(new Intl.NumberFormat('pt-BR', {
        style: 'currency',
        currency: 'BRL'
    }).format(valor_pago));

    if (valor_pago > 0) {
        troco = parseFloat(valor_pago) - parseFloat(valor_total_venda);
        // if (troco > 0) {
        $('#troco_modal_pay').html(new Intl.NumberFormat('pt-BR', {
            style: 'currency',
            currency: 'BRL'
        }).format(troco));
        // }
    }
}

async function vender() {
    if (parseFloat(valor_pago) + parseFloat(desconto) >= parseFloat($('#valorTotalVendaInput').val())) {

        for (var i = 0; i < formas_pag.length; i++) {
            if (formas_pag[i] == "x") {
                formas_pag.splice(i, 1);
                valores_pag.splice(i, 1);
            }
        }

        var cliente = $('#cli_pdv').val();

        await axios({
            method: 'POST',
            url: 'http://localhost:3000/pdvCreate',
            data: {
                cliente,
                prods,
                qtds,
                precos,
                formas_pag,
                valores_pag,
                desconto,
                troco
            },
            headers: {}
        })
            .then(function (response) {
                // $('#ForPag').modal('hide');
                // $('#confirmacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(function (response) {
                alert("Erro!");
                console.log(error);
                // $('#ForPag').modal('hide');
                // $('#negacaoCadastro').modal('show');
                // setTimeout(
                //     function () {
                //         location.reload();
                //     }, 2000
                // );
            });

        // console.log("-----PRODS----");
        // console.log(prods);
        // console.log("-----QTDS----");
        // console.log(qtds);
        // console.log("-----PRECOS----");
        // console.log(precos);
        // console.log("-----FORMAS_PAG----");
        // console.log(formas_pag);
        // console.log("----VALORES_PAG-----");
        // console.log(valores_pag);
        // console.log("-----VALOR_PAGO----");
        // console.log(valor_pago);
        // console.log("-----DESCONTO----");
        // console.log(desconto);
        // console.log("-----TROCO----");
        // console.log(troco);
        // console.log("-----VALOR_TOTAL_VENDA_INPUT----");
        // console.log(parseFloat($('#valorTotalVendaInput').val()));
        // console.log("-----FIM----");
    } else {
        alert("Complete corretamente os valores!");
    }
}

function abrir_caixa() {
    $('#modalCaixa').modal('show');
}
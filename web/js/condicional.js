var quantidade_produto = 1;
	axios.get('http://localhost:3000/condicionais').then(resp => {
		var tbody = document.getElementById('conteudo-condicionais');
		resp.data.forEach(function (value) {
			var tr = document.createElement('tr');
			var id = value.id;
			var td_cupom = document.createElement('td');
			var td_cliente = document.createElement('td');
			var td_data_saida = document.createElement('td');
			var td_data_volta = document.createElement('td');
			var td_valor = document.createElement('td');
			var td_confirmar = document.createElement('td');

			td_cupom.innerHTML =
				"<center>" +
				"<button class='btn btn-primary btn-circle' onclick='view(" + id + ")' >" +
				"<i class='fas fa-eye'></i>" +
				"</button>" +
				"</center>";

			td_cliente.innerHTML = value.cliente;
			td_data_saida.innerHTML = value.data_saida;
			td_data_volta.innerHTML = value.data_volta;
			td_valor.innerHTML = value.valor_condicional;

			td_confirmar.innerHTML =
				"<center>" +
				"<button class='btn btn-success btn-circle' onclick='confirm(" + id + ")' >" +
				"<i class='fas fa-check'></i>" +
				"</button>" +
				"</center>";

			tr.appendChild(td_cupom);
			tr.appendChild(td_cliente);
			tr.appendChild(td_data_saida);
			tr.appendChild(td_data_volta);
			tr.appendChild(td_valor);
			tr.appendChild(td_confirmar);

			tbody.appendChild(tr);
		});

		$(document).ready(function () {
			$('#dataTable').DataTable({});
		});
	});


	function addCondicional() {
		$('#AddCon').modal('show');

		var aux_select_cli = 0;
		var select_cli = document.getElementById('cli_con');
		select_cli.innerText = "";

		axios.get('http://localhost:3000/clientes').then(resp => {
			resp.data.forEach(function (value) {

				if (aux_select_cli == 0) {
					var cli_opt_ini = document.createElement("option");
					var cli_txt_ini = document.createTextNode("Selecione o cliente");

					cli_opt_ini.appendChild(cli_txt_ini);
					select_cli.appendChild(cli_opt_ini);

					aux_select_cli++;
				}

				var cli_opt = document.createElement("option");
				cli_opt.setAttribute("value", value.id);

				var cli_txt = document.createTextNode(value.nome);
				cli_opt.appendChild(cli_txt);

				select_cli.appendChild(cli_opt);
			});
		});

		var aux_select_prod = 0;
		var select_prod = document.getElementById('prod_con_0');
		select_prod.innerText = "";

		axios.get('http://localhost:3000/produtosSelect').then(resp => {
			resp.data.forEach(function (value) {

				if (aux_select_prod == 0) {
					var prod_opt_ini = document.createElement("option");
					var prod_txt_ini = document.createTextNode("Selecione o produto");

					prod_opt_ini.appendChild(prod_txt_ini);
					select_prod.appendChild(prod_opt_ini);

					aux_select_prod++;
				}

				var prod_opt = document.createElement("option");
				prod_opt.setAttribute("value", value.id);

				var prod_txt = document.createTextNode(value.nome + " - " + value.tamanho + " (" + value.genero + ")");
				prod_opt.appendChild(prod_txt);

				select_prod.appendChild(prod_opt);
			});
		});
	};

	async function cadastrar_condicional() {
		if ($('#cli_con').val() != "" && $('#data_saida_con').val() != "") {
			var count_prod = $('#qtd_produto').val();
			var count_prod_aux = count_prod;

			var cliente = $('#cli_con').val();
			var data_saida = $('#data_saida_con').val();

			var produtos = [];
			var qtds = [];

			produtos.push($('#prod_con_0').val());
			qtds.push($('#qtd_con_0').val());

			if (count_prod_aux > 1) {
				for (var i = 1; i < count_prod_aux; i++) {
					var prod = $('#prod_con_' + i).val();
					var qtd = $('#qtd_con_' + i).val();
					if (prod != undefined && qtd != undefined) {
						produtos.push(prod);
						qtds.push(qtd);
					} else {
						count_prod_aux++;
					}
				}
			}
			console.log(produtos);
			console.log(qtds);

			await axios({
				//tras todos as plenarias no option select
				method: 'POST',
				url: 'http://localhost:3000/condicionaisCreate',
				data: {
					cliente,
					count_prod,
					produtos,
					qtds,
					data_saida
				},
				headers: {}
			})
				.then(function (response) {
					$('#AddCon').modal('hide');
					$('#confirmacaoCadastro').modal('show');
					setTimeout(
						function () {
							window.location = 'views/cupom_condicional.html';
						}, 2000
					);
				})
				.catch(function (response) {
					console.log(error);
					$('#AddCon').modal('hide');
					$('#negacaoCadastro').modal('show');
					setTimeout(
						function () {
							// location.reload();
						}, 2000
					);
				});
		} else {
			alert("Campos incompletos")
		}
	};

	function confirm(id) {
		$('#ConfCon').modal('show');
	}

	function editar_tamanho() {
		if ($('#tamanho_tam_edit').val() != "" && $('#tipo_tam_edit').val() != "") {

			var id = $('#id_tam_edit').val();
			var tamanho = $('#tamanho_tam_edit').val();
			var tipo = $('#tipo_tam_edit').val();

			axios.put('http://localhost:3000/tamanhosEdit?id=' + id, {
				tamanho,
				tipo
			})
				.then(response => {
					console.log(response);
					$('#EditTam').modal('hide');
					$('#confirmacaoEditar').modal('show');
					setTimeout(
						function () {
							location.reload();
						}, 2000
					);
				})
				.catch(error => {
					console.log(error);
				});
		} else {
			alert("Campos incompletos")
		}
	}

	var quantidade_produto = 1;
	var ids = 1;

	function novo_produto() {
		var div_row = document.createElement("div");
		div_row.setAttribute('class', 'form-row marginb');
		div_row.setAttribute('id', 'div-produto-' + ids);

		div_row.innerHTML = "<div class='col-8'><select name='prod_con_" + ids + "' id='prod_con_" + ids + "' class='form-control' onchange='ajustar_quantidade(this)'></select></div><div class='col-3'><input name='qtd_con_" + ids + "' id='qtd_con_" + ids + "' type='number' step='1' class='form-control' readonly='readonly' onchange='verificar_quantidade(this)' placeholder='Qtd'/></div><div class='col-1'><button class='btn btn-danger' type='button' onclick='remover_produto(" + ids + ")'><i class='fas fa-minus'></i></button></div>";
		document.getElementById('div-produtos').appendChild(div_row);

		var aux_select_prod = 0;
		var select_prod = document.getElementById('prod_con_' + ids);
		axios.get('http://localhost:3000/produtosSelect').then(resp => {
			resp.data.forEach(function (value) {

				if (aux_select_prod == 0) {
					var prod_opt_ini = document.createElement("option");
					var prod_txt_ini = document.createTextNode("Selecione o produto");

					prod_opt_ini.appendChild(prod_txt_ini);
					select_prod.appendChild(prod_opt_ini);

					aux_select_prod++;
				}

				var prod_opt = document.createElement("option");
				prod_opt.setAttribute("value", value.id);

				var prod_txt = document.createTextNode(value.nome + " - " + value.tamanho + " (" + value.genero + ")");
				prod_opt.appendChild(prod_txt);

				select_prod.appendChild(prod_opt);
			});
		});

		quantidade_produto++;
		ids++;

		document.getElementById('qtd_produto').value = quantidade_produto;
	}

	function remover_produto(id) {
		var div = document.getElementById('div-produto-' + id);
		div.parentNode.removeChild(div);
		quantidade_produto--;
		document.getElementById('qtd_produto').value = quantidade_produto;
	}

	function ajustar_quantidade(obj) {
		var qtd_prod_aux = quantidade_produto;
		var igual = 0;
		var n = obj.id.split("_");

		for (var i = 0; i < qtd_prod_aux; i++) {
			if ($('#prod_con_' + i)) {
				if ($('#prod_con_' + i).val() == obj.value && obj.id != "prod_con_" + i) {
					igual = 1;
				}
			} else {
				qtd_prod_aux++;
			}
		}

		if (igual == 0) {
			axios.get('http://localhost:3000/produtosGetQtd?id=' + obj.value).then(resp => {
				resp.data.forEach(function (value) {
					$('#qtd_con_' + n[2]).removeAttr('readonly');
					$('#qtd_con_' + n[2]).val(value.quantidade);
				});
			});
		} else {
			alert("Produto já selecionado!");
			$('#prod_con_' + n[2]).val("");
		}
	}

	function verificar_quantidade(obj) {
		var n = obj.id.split("_");
		var id_prod = $('#prod_con_' + n[2]).val();

		axios.get('http://localhost:3000/produtosGetQtd?id=' + id_prod).then(resp => {
			resp.data.forEach(function (value) {
				if (obj.value > value.quantidade) {
					alert('Quantidade máxima ultrapassada! Ajustado para a máxima possível.');
					$('#qtd_con_' + n[2]).val(value.quantidade);
				}
			});
		});
	}
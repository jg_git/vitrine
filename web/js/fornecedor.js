$(document).ready(function () {
    $("#buscaCep").click(function () {

        //Nova variável "cep" somente com dígitos.
        var cep = $("#cep_for").val().replace(/\D/g, '');

        //Verifica se campo cep possui valor informado.
        if (cep != "") {

            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;

            //Valida o formato do CEP.
            if (validacep.test(cep)) {

                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#endereco_for").val(dados.logradouro);
                        $("#endereco_for").css("background", "#eee");
                        $("#bairro_for").val(dados.bairro);
                        $("#bairro_for").css("background", "#eee");
                        $("#cidade_for").val(dados.localidade);
                        $("#cidade_for").css("background", "#eee");
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                alert("Formato de CEP inválido.");
            }
        } //end if.
    });
});

axios.get('http://localhost:3000/fornecedores').then(resp => {
    var tbody = document.getElementById('conteudo-fornecedores');
    resp.data.forEach(function (value) {
        var tr = document.createElement('tr');
        var id = value.id;
        var td_nome = document.createElement('td');
        var td_cnpj = document.createElement('td');
        var td_endereco = document.createElement('td');
        var td_tel = document.createElement('td');
        var td_editar = document.createElement('td');

        td_nome.innerHTML = value.nome;
        td_cnpj.innerHTML = value.cnpj;
        td_endereco.innerHTML = value.endereco + ", "
            + value.numero + " - "
            + value.bairro + " "
            + value.cidade;
        td_tel.innerHTML = value.telefone;

        td_editar.innerHTML =
            "<center>" +
            "<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
            "<i class='fas fa-edit' ></i>" +
            "</button>" +
            "</center>";

        tr.appendChild(td_nome);
        tr.appendChild(td_cnpj);
        tr.appendChild(td_endereco);
        tr.appendChild(td_tel);
        tr.appendChild(td_editar);

        tbody.appendChild(tr);
    });

    $(document).ready(function () {
        $('#dataTable').DataTable({});
    });
});


function addFornecedor() {
    $('#AddFor').modal('show');
};

async function cadastrar_fornecedor() {
    if ($('#nome_for').val() != "" && $('#cnpj_for').val() != "" &&
        $('#cep_for').val() != "" && $('#endereco_for').val() != "" &&
        $('#numero_for').val() != "" && $('#bairro_for').val() != "" &&
        $('#cidade_for').val() != "" && $('#telefone_for').val() != "") {
        var nome = $('#nome_for').val();
        var cnpj = $('#cnpj_for').val();
        var cep = $('#cep_for').val();
        var endereco = $('#endereco_for').val();
        var numero = $('#numero_for').val();
        var bairro = $('#bairro_for').val();
        var cidade = $('#cidade_for').val();
        var telefone = $('#telefone_for').val();

        await axios({
            //tras todos as plenarias no option select
            method: 'POST',
            url: 'http://localhost:3000/fornecedoresCreate',
            data: {
                nome,
                cnpj,
                cep,
                endereco,
                numero,
                bairro,
                cidade,
                telefone
            },
            headers: {}
        })
            .then(function (response) {
                // location.reload();
                $('#AddFor').modal('hide');
                $('#confirmacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(function (response) {
                console.log(error);
                $('#AddFor').modal('hide');
                $('#negacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            });
    }else{
        alert("Campos incompletos");
    }
};

function edit(id) {
    $('#EditFor').modal('show');
    axios.get('http://localhost:3000/fornecedoresGetEdit?id=' + id).then(resp => {
        resp.data.forEach(function (value) {
            $('#id_for_edit').val(value.id);
            $('#nome_for_edit').val(value.nome);
            $('#cnpj_for_edit').val(value.cnpj);
            $('#telefone_for_edit').val(value.telefone);
            $('#cep_for_edit').val(value.cep);
            $('#endereco_for_edit').val(value.endereco);
            $('#numero_for_edit').val(value.numero);
            $('#bairro_for_edit').val(value.bairro);
            $('#cidade_for_edit').val(value.cidade);
        });
    });
}

function editar_fornecedor() {
    if ($('#nome_for_edit').val() != "" && $('#cnpj_for_edit').val() != "" &&
        $('#cep_for_edit').val() != "" && $('#endereco_for_edit').val() != "" &&
        $('#numero_for_edit').val() != "" && $('#bairro_for_edit').val() != "" &&
        $('#cidade_for_edit').val() != "" && $('#telefone_for_edit').val() != "") {

        var id = $('#id_for_edit').val();
        var nome = $('#nome_for_edit').val();
        var cnpj = $('#cnpj_for_edit').val();
        var cep = $('#cep_for_edit').val();
        var endereco = $('#endereco_for_edit').val();
        var numero = $('#numero_for_edit').val();
        var bairro = $('#bairro_for_edit').val();
        var cidade = $('#cidade_for_edit').val();
        var telefone = $('#telefone_for_edit').val();

        axios.put('http://localhost:3000/fornecedoresEdit?id=' + id, {
            nome,
            cnpj,
            cep,
            endereco,
            numero,
            bairro,
            cidade,
            telefone
        })
            .then(response => {
                console.log(response);
                $('#EditFor').modal('hide');
                $('#confirmacaoEditar').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(error => {
                console.log(error);
            });
    }else{
        alert("Campos incompletos");
    }
}
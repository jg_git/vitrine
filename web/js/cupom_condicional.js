$(document).ready(function () {
    axios.get('http://localhost:3000/empresasGetEdit').then(resp => {
        resp.data.forEach(function (value) {
            var endereco = value.endereco + " Nº " + value.numero + ", " + value.bairro + " - " + value.cidade;
            var cnpj = "<b>CNPJ: </b>"+value.cnpj;
            var ie = "<b>IE: </b>"+value.ie;
            $('#endereco_cup_con').html(endereco);
            $('#tel_cup_con').html(value.telefone);
            $('#cnpj_cup_con').html(cnpj);
            $('#ie_cup_con').html(ie);
            document.getElementById('logo_cup_con').setAttribute('src', value.logo_cupom);
        });
    });
});
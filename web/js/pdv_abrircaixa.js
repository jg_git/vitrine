$(document).ready(function () {
    var id_caixa;
    axios.get('http://localhost:3000/pdvStatusCaixa').then(resp => {
        resp.data.forEach(function (value) {
            id_caixa = value.id;
        });

        if (id_caixa > 0) {
            window.location.href = 'index.html?id_caixa='+id_caixa;
        }
    });
});
function abrir_caixa() {
    $('#modalAbrirCaixa').modal('show');
}

async function abrir() {
    if ($('#valor_caixa').val() != "") {
        var valor_caixa = $('#valor_caixa').val();

        await axios({
            //tras todos as plenarias no option select
            method: 'POST',
            url: 'http://localhost:3000/pdvAbrirCaixa',
            data: {
               valor_caixa
            },
            headers: {}
        }).then(function (response) {
            $('#modalAbrirCaixa').modal('hide');
            $('#confirmacaoCaixa').modal('show');
            setTimeout(
                function () {
                    
                    id_caixa = response.data[0];
                    window.location.href = 'index.html?id_caixa='+id_caixa;
                }, 2000
            );
        }).catch(function (response) {
            console.log(error);
            $('#modalAbrirCaixa').modal('hide');
            $('#negacaoCaixa').modal('show');
            setTimeout(
                function () {
                    location.reload();
                }, 2000
            );
        });
    } else {
        alert('Insira um valor inicial para o caixa!');
    }
}
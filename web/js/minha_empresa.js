var base64_emp_1;
    var base64_emp_2;
    var base64_emp_3;

    axios.get('http://localhost:3000/empresasGetEdit').then(resp => {
        resp.data.forEach(function (value) {
            $('#razao_social').val(value.razao_social);
            $('#nome_fantasia').val(value.nome_fantasia);
            $('#email').val(value.email);
            $('#cnpj').val(value.cnpj);
            $('#ie').val(value.ie);
            $('#telefone').val(value.telefone);
            $('#endereco').val(value.endereco);
            $('#numero').val(value.numero);
            $('#bairro').val(value.bairro);
            $('#cidade').val(value.cidade);
            document.getElementById('show-img-emp-1').setAttribute('src', value.logo_principal_g);
            document.getElementById('show-img-emp-2').setAttribute('src', value.logo_principal_p);
            document.getElementById('show-img-emp-3').setAttribute('src', value.logo_cupom);
        });
    });

    $(document).ready(function () {
        $('#falseinput_emp_1').click(function () {
            $("#img_emp_1").click();
        });
        $('#falseinput_emp_2').click(function () {
            $("#img_emp_2").click();
        });
        $('#falseinput_emp_3').click(function () {
            $("#img_emp_3").click();
        });
    });

    $('#img_emp_1').change(function () {
        $('#selected_filename_emp_1').html($('#img_emp_1')[0].files[0].name);
        $('#div-img-emp-1').removeClass("hide").addClass("show");
    });


    $('#img_emp_2').change(function () {
        $('#selected_filename_emp_2').html($('#img_emp_2')[0].files[0].name);
        $('#div-img-emp-2').removeClass("hide").addClass("show");
    });

    $('#img_emp_3').change(function () {
        $('#selected_filename_emp_3').html($('#img_emp_3')[0].files[0].name);
        $('#div-img-emp-3').removeClass("hide").addClass("show");
    });

    function mostrarimg(obj, edit) {
        var file = document.getElementById(obj.id).files;
        var n = obj.id;
        n = n.split("_");

        if (file.length > 0) {
            var reader = new FileReader();

            reader.onload = function (e) {
                if (n[2] == 1) {
                    base64_emp_1 = e.target.result;
                } else if (n[2] == 2) {
                    base64_emp_2 = e.target.result;
                } else if (n[2] == 3) {
                    base64_emp_3 = e.target.result;
                }

                document.getElementById('show-img-emp-' + n[2]).setAttribute("src", e.target.result);
            }

            reader.readAsDataURL(file[0]);
        }
    }

    function editar_empresa() {
        if ($('#razao_social').val() != "" && $('#nome_fantasia').val() != "" &&
            $('#email').val() != "" && $('#cnpj').val() != "" && $('#ie').val() != "" &&
            $('#telefone').val() != "" && $('#endereco').val() != "" && $('#numero').val() != "" &&
            $('#bairro').val() != "" && $('#cidade').val() != "") {
            var resp = confirm("Deseja alterar os dados?");
            if (resp) {
                var razao_social = $('#razao_social').val();
                var nome_fantasia = $('#nome_fantasia').val();
                var email = $('#email').val();
                var cnpj = $('#cnpj').val();
                var ie = $('#ie').val();
                var telefone = $('#telefone').val();
                var endereco = $('#endereco').val();
                var numero = $('#numero').val();
                var bairro = $('#bairro').val();
                var cidade = $('#cidade').val();

                var img_1 = $('#img_emp_1').val();
                var img_2 = $('#img_emp_2').val();
                var img_3 = $('#img_emp_3').val();

                if ($('#img_emp_1').val() == "") {
                    base64_emp_1 = document.getElementById('show-img-emp-1').src;
                }

                if ($('#img_emp_2').val() == "") {
                    base64_emp_2 = document.getElementById('show-img-emp-2').src;
                }

                if ($('#img_emp_3').val() == "") {
                    base64_emp_3 = document.getElementById('show-img-emp-3').src;
                }

                axios.put('http://localhost:3000/empresasEdit', {
                    razao_social,
                    nome_fantasia,
                    email,
                    cnpj,
                    ie,
                    telefone,
                    endereco,
                    numero,
                    bairro,
                    cidade,
                    img_1: base64_emp_1,
                    img_2: base64_emp_2,
                    img_3: base64_emp_3
                })
                    .then(response => {
                        $('#confirmacaoEditar').modal('show');
                        setTimeout(
                            function () {
                                location.reload();
                            }, 2000
                        );
                    })
                    .catch(error => {
                        console.log(error);
                    });
            }
        } else {
            alert("Campos incompletos!");
        }
    }
var base64_subcat;

	$(document).ready(function () {
		$('#falseinput').click(function () {
			$("#img_sub").click();
		});
	});
	$('#img_sub').change(function () {
		$('#selected_filename').html($('#img_sub')[0].files[0].name);
		$('#div-img').removeClass("hide").addClass("show");
	});

	$(document).ready(function () {
		$('#falseinput_edit').click(function () {
			$("#img_sub_edit").click();
		});
	});
	$('#img_sub_edit').change(function () {
		$('#selected_filename_edit').html($('#img_sub_edit')[0].files[0].name);
		$('#div-img-edit').removeClass("hide").addClass("show");
	});

	subcategorias();
	async function subcategorias(){
	
	await axios({
	url:'http://localhost:3003/subcategorias',
	method: 'GET',
	headers:{'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}
	})
	
	.then(resp => {
		var tbody = document.getElementById('conteudo-subcategorias');
		// console.log(resp);
		resp.data.forEach(function (value) {
			
			var tr = document.createElement('tr');
			var id = value.id;
			var td_titulo = document.createElement('td');
			var td_categoriaId = document.createElement('td');
			var td_editar = document.createElement('td');
			console.log(value);

			td_titulo.innerHTML = value.titulo;
			td_categoriaId.innerHTML = value.Categorium.titulo;

			td_editar.innerHTML =
				"<center>" +
				"<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
				"<i class='fas fa-edit' ></i>" +
				"</button>" +
				"</center>";

			tr.appendChild(td_titulo);
			tr.appendChild(td_categoriaId);
			tr.appendChild(td_editar);

			tbody.appendChild(tr);
		});

		$(document).ready(function () {
			$('#dataTable').DataTable({});
		});
	});
}


	async function addSubcategoria() {
		$('#AddSub').modal('show');
		var select = document.getElementById("cat_sub");
		select.innerText = "";
		
		await axios({
			url: `http://localhost:3003/categorias`,
			method: 'GET',
			headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}
		})
		.then(resp => {
			resp.data.forEach(function (value) {
				var x = document.createElement("option");
				x.setAttribute("value", value.id);
				var t = document.createTextNode(value.titulo);
				x.appendChild(t);
				
				console.log(value.titulo);
				

				select.appendChild(x);
				select.removeAttribute("readonly");

			});
		});
	};

	async function cadastrar_subcategoria() {
		if ($('#nome_sub').val() != "" && $('#descricao_sub').val() != "") {
			var titulo = $('#nome_sub').val();
			var descricao = $('#descricao_sub').val();
			var categoria = $('#cat_sub').val();
			var imagem = $('#img_sub').val();

			console.log(categoria);

			await axios({
				//tras todos as plenarias no option select
				method: 'POST',
				url: `http://localhost:3003/subcategorias/${categoria}`,
				headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
				data: {
					titulo,
					id_categoria: categoria,
					descricao,
					imagem
				},
			})
				.then(function (response) {
					// location.reload();
					$('#AddSub').modal('hide');
					$('#confirmacaoCadastro').modal('show');
					setTimeout(
						function () {
							location.reload();
						}, 2000
					);
				})
				.catch(function (response) {
					if(response.status == 406){
						$('#AddSub').modal('hide');
						$('#negacaoCadastro406').modal('show');
						setTimeout(
							function () {
								location.reload();
							}, 2000
						);
					}else{
						$('#AddSub').modal('hide');
						$('#negacaoCadastro').modal('show');
						setTimeout(
							function () {
								location.reload();
							}, 2000
						);
					}

				});
		} else {
			alert("Campos incompletos");
		}
	};

	async function edit(id) {
		$('#EditSub').modal('show');

		var select = document.getElementById("cat_sub_edit");
		select.innerText = "";

		var categoria = $('#cat_sub').val();

		await axios({
			url: `http://localhost:3003/categorias`,
			method: 'GET',
			headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}
		})
		.then(resp => {
			resp.data.forEach(function (value) {
				var x = document.createElement("option");
				x.setAttribute("value", value.id);
				var t = document.createTextNode(value.titulo);
				x.appendChild(t);

				select.appendChild(x);
				select.removeAttribute("readonly");

				console.log(x);
			});
		});

		await axios({
		url: `http://localhost:3003/subcategorias/${categoria}`,
		method: 'GET',
		headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}
		}
			).then(resp => {
			resp.data.forEach(function (value) {
				console.log(value);
				$('#id_sub_edit').val(value.id);
				$('#nome_sub_edit').val(value.titulo);
				$('#descricao_sub_edit').val(value.descricao);
				$('#cat_sub_edit').val(value.categoriaId);
				document.getElementById('show-img-edit').setAttribute('src',value.img);
			});
		});
	}

	async function editar_subcategoria() {
		if ($('#nome_sub_edit').val() != "" && $('#cat_sub_edit').val()) {
			var id = $('#id_sub_edit').val();
			var titulo = $('#nome_sub_edit').val();
			var descricao = $('#descricao_sub_edit').val();
			var categoria = $('#cat_sub_edit').val();
			var img = $('#img_sub_edit').val();

			if(img != ""){
				base64_subcat = document.getElementById('show-img-edit').src;
			}

			await axios({
				url: `http://localhost:3003/subcategorias/${categoria}`,
				method: 'PUT',
				headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
				data: {
					titulo,
					id_categoria: categoria,
					descricao,
					img
				}
			})
				.then(response => {
					console.log(response);
					$('#EditSub').modal('hide');
					$('#confirmacaoEditar').modal('show');
					setTimeout(
						function () {
							location.reload();
						}, 2000
					);
				})
				.catch(error => {
					console.log(error);
				});
		} else {
			alert("Campos incompletos");
		}
	}

	function mostrarimg(obj,edit) {

		var file = document.getElementById(obj.id).files;

		if (file.length > 0) {
			var reader = new FileReader();

			reader.onload = function (e) {
				if(edit==0){
					base64_subcat = e.target.result;
					document.getElementById('show-img').setAttribute("src", e.target.result);
				}else if(edit==1){
					base64_subcat = e.target.result;
					document.getElementById('show-img-edit').setAttribute("src", e.target.result);
				}
			}

			reader.readAsDataURL(file[0]);
		}
	}
$(document).ready(function () {
    $('#falseinput').click(function () {
        $("#img_mar").click();
    });
});
$('#img_mar').change(function () {
    $('#selected_filename').html($('#img_mar')[0].files[0].name);
    $('#div-img').removeClass("hide").addClass("show");
});

$(document).ready(function () {
    $('#falseinput_edit').click(function () {
        $("#img_mar_edit").click();
    });
});
$('#img_mar_edit').change(function () {
    $('#selected_filename_edit').html($('#img_mar_edit')[0].files[0].name);
});

var base64_marca;
function mostrarimg(obj, edit) {
    
    var file = document.getElementById(obj.id).files;
    
    if (file.length > 0) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            if (edit == 0) {
                base64_marca = e.target.result;
                document.getElementById('show-img').setAttribute("src", e.target.result);
            } else if (edit == 1) {
                base64_marca = e.target.result;
                document.getElementById('show-img-edit').setAttribute("src", e.target.result);
            }
        }
        
        reader.readAsDataURL(file[0]);
    }
}

marcas();
async function marcas(){
    await axios({
        method: 'GET',
        url: 'http://localhost:3003/marcas', 
        headers:{'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}}
        ).then(resp => {
        var tbody = document.getElementById('conteudo-marcas');
        resp.data.forEach(function (value) {
            var tr = document.createElement('tr');
            var id = value.id;
            var td_titulo = document.createElement('td');
            var td_editar = document.createElement('td');
            var td_excluir = document.createElement('td');

            td_titulo.innerHTML = value.titulo;

            td_editar.innerHTML =
                "<center>" +
                "<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
                "<i class='fas fa-edit' ></i>" +
                "</button>" +
                "</center>";
            
            td_excluir.innerHTML =
                "<center>" +
                "<button class='btn btn-danger btn-circle' onclick='excluir_marcas(" + id + ")'>" +
                "<i class='fas fa-trash' ></i>" +
                "</button>" +
                "</center>";
            
            tr.appendChild(td_titulo);
            tr.appendChild(td_editar);
            tr.appendChild(td_excluir);

            tbody.appendChild(tr);
        });

        $(document).ready(function () {
            $('#dataTable').DataTable({});
        });
    });
}
        
        
function addMarca() {
$('#AddMar').modal('show');
};

async function cadastrar_marca() {
if ($('#nome_mar').val() != "" && $('#descricao_mar').val() != "") {
    var titulo = $('#nome_mar').val();
    var descricao = $('#descricao_mar').val();
    var logo = $('#img_mar').val();
    
    await axios({
        //tras todos as plenarias no option select
        method: 'POST',
        url: 'http://localhost:3003/marcas',
        headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
        data: {
            titulo,
            descricao,
            logo
        },
    })
    .then(function (response) {
        // location.reload();
        $('#AddMar').modal('hide');
        $('#confirmacaoCadastro').modal('show');
        setTimeout(
            function () {
                location.reload();
            }, 2000
            );
        })
        .catch(function (response) {
            $('#AddMar').modal('hide');
            $('#negacaoCadastro').modal('show');
            setTimeout(
                function () {
                    location.reload();
                }, 2000
                );
            });
        } else {
            alert("Campos incompletos");
        }
    };
    
async function edit(id) {
$('#EditMar').modal('show');
const {data, status} = await axios({
    method:"GET",
    url: `http://localhost:3003/marcas/${id}`,
    headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
});
    if(status == 200){
        console.log(data.titulo);
            $('#id_mar_edit').val(data.id);
            $('#nome_mar_edit').val(data.titulo);
            $('#descricao_mar_edit').val(data.descricao)
            //$('#img_cat_edit').val(value.imagem);
            document.getElementById('show-img-edit').setAttribute('src',data.logo);
    }
}
    
async function editar_marca() {
if ($('#nome_mar_edit').val() != "") {
    var id = $('#id_mar_edit').val();
    var titulo = $('#nome_mar_edit').val();
    var descricao = $('#descricao_mar_edit').val();
    var logo = $('#img_mar_edit').val();
    
    // if (logo == "") {
    //     base64_marca = document.getElementById('show-img-edit').src;
    // }
    
    await axios({
        method: 'PUT',
        url:  `http://localhost:3003/marcas/${id}`,
        headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
        data: {
            titulo,
            descricao,
            logo
        }   
    })
    .then(response => {
        console.log(response);
        $('#EditMar').modal('hide');
        $('#confirmacaoEditar').modal('show');
        setTimeout(
            function () {
                location.reload();
            }, 2000
            );
        })
        .catch(error => {
            console.log(error);
        });
    } else {
        alert("Campos incompletos");
    }
}

function delCategoria(id) {
    $('#DelMar').modal('show');
};

async function excluir_marcas(id){
    const response = await axios({
        url: `http://localhost:3003/marcas/${id}`,
        method : "DELETE",
        headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}
    })
    if(response.status == 200){
        $('#confirmacaoExcluir').modal('show');
        setTimeout(
            function () {
                location.reload();
            }, 2000
        );
    }else{
        console.log(response);
    }
}
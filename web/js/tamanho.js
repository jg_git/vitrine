$(document).ready(function () {
    $("#buscaCep").click(function () {
        
        //Nova variável "cep" somente com dígitos.
        var cep = $("#cep_for").val().replace(/\D/g, '');
        
        //Verifica se campo cep possui valor informado.
        if (cep != "") {
            
            //Expressão regular para validar o CEP.
            var validacep = /^[0-9]{8}$/;
            
            //Valida o formato do CEP.
            if (validacep.test(cep)) {
                
                //Consulta o webservice viacep.com.br/
                $.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {
                    
                    if (!("erro" in dados)) {
                        //Atualiza os campos com os valores da consulta.
                        $("#endereco_for").val(dados.logradouro);
                        $("#endereco_for").css("background", "#eee");
                        $("#bairro_for").val(dados.bairro);
                        $("#bairro_for").css("background", "#eee");
                        $("#cidade_for").val(dados.localidade);
                        $("#cidade_for").css("background", "#eee");
                    } //end if.
                    else {
                        //CEP pesquisado não foi encontrado.
                        alert("CEP não encontrado.");
                    }
                });
            } //end if.
            else {
                alert("Formato de CEP inválido.");
            }
        } //end if.
    });
});

axios.get('http://localhost:3000/tamanhos').then(resp => {
    var tbody = document.getElementById('conteudo-tamanhos');
    resp.data.forEach(function (value) {
        var tr = document.createElement('tr');
        var id = value.id;
        var td_tamanho = document.createElement('td');
        var td_tipo = document.createElement('td');
        var td_editar = document.createElement('td');

        td_tamanho.innerHTML = value.tamanho;
        td_tipo.innerHTML = value.tipo;

        td_editar.innerHTML =
            "<center>" +
            "<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
            "<i class='fas fa-edit' ></i>" +
            "</button>" +
            "</center>";

        tr.appendChild(td_tamanho);
        tr.appendChild(td_tipo);
        tr.appendChild(td_editar);

        tbody.appendChild(tr);
    });

    $(document).ready(function () {
        $('#dataTable').DataTable({});
    });
});


function addTamanho() {
    $('#AddTam').modal('show');
};

async function cadastrar_tamanho() {
    if ($('#tamanho_tam').val() != "" && $('#tipo_tam').val() != "") {

        var tamanho = $('#tamanho_tam').val();
        var tipo = $('#tipo_tam').val();

        await axios({
            //tras todos as plenarias no option select
            method: 'POST',
            url: 'http://localhost:3000/tamanhosCreate',
            data: {
                tamanho,
                tipo
            },
            headers: {}
        })
            .then(function (response) {
                // location.reload();
                $('#AddTam').modal('hide');
                $('#confirmacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(function (response) {
                console.log(error);
                $('#AddTam').modal('hide');
                $('#negacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            });
    } else {
        alert("Campos incompletos")
    }
};

function edit(id) {
    $('#EditTam').modal('show');
    axios.get('http://localhost:3000/tamanhosGetEdit?id=' + id).then(resp => {
        resp.data.forEach(function (value) {
            $('#id_tam_edit').val(value.id);
            $('#tamanho_tam_edit').val(value.tamanho);
            $('#tipo_tam_edit').val(value.id_tipo_tamanho);
        });
    });
}

function editar_tamanho() {
    if ($('#tamanho_tam_edit').val() != "" && $('#tipo_tam_edit').val() != "") {

        var id = $('#id_tam_edit').val();
        var tamanho = $('#tamanho_tam_edit').val();
        var tipo = $('#tipo_tam_edit').val();

        axios.put('http://localhost:3000/tamanhosEdit?id=' + id, {
            tamanho,
            tipo
        })
            .then(response => {
                console.log(response);
                $('#EditTam').modal('hide');
                $('#confirmacaoEditar').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(error => {
                console.log(error);
            });
    } else {
        alert("Campos incompletos")
    }
}
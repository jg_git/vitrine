$(document).ready(function () {
    $('#falseinput').click(function () {
        $("#img_cat").click();
    });
});
$('#img_cat').change(function () {
    $('#selected_filename').html($('#img_cat')[0].files[0].name);
    $('#div-img').removeClass("hide").addClass("show");
});

$(document).ready(function () {
    $('#falseinput_edit').click(function () {
        $("#img_cat_edit").click();
    });
});
$('#img_cat_edit').change(function () {
    $('#selected_filename_edit').html($('#img_cat_edit')[0].files[0].name);
});

var base64;
function mostrarimg(obj, edit) {

    var file = document.getElementById(obj.id).files;

    if (file.length > 0) {
        var reader = new FileReader();

        reader.onload = function (e) {
            if (edit == 0) {
                base64 = e.target.result;
                document.getElementById('show-img').setAttribute("src", e.target.result);
            } else if (edit == 1) {
                base64 = e.target.result;
                document.getElementById('show-img-edit').setAttribute("src", e.target.result);
            }
        }

        reader.readAsDataURL(file[0]);
    }
}

teste();
async function teste(){
    await axios({
        method: 'GET',
        url: 'http://localhost:3003/categorias', 
        headers:{'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}}
        ).then(resp => {
        var tbody = document.getElementById('conteudo-categorias');
        resp.data.forEach(function (value) {
            var tr = document.createElement('tr');
            var id = value.id;
            var td_titulo = document.createElement('td');
            var td_editar = document.createElement('td');
            var td_excluir = document.createElement('td');

            td_titulo.innerHTML = value.titulo;

            td_editar.innerHTML =
                "<center>" +
                "<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
                "<i class='fas fa-edit' ></i>" +
                "</button>" +
                "</center>";
            
            td_excluir.innerHTML =
                "<center>" +
                "<button class='btn btn-danger btn-circle' onclick='excluir_categoria(" + id + ")'>" +
                "<i class='fas fa-trash' ></i>" +
                "</button>" +
                "</center>";
            
            tr.appendChild(td_titulo);
            tr.appendChild(td_editar);
            tr.appendChild(td_excluir);

            tbody.appendChild(tr);
        });

        $(document).ready(function () {
            $('#dataTable').DataTable({});
        });
    });
}


function addCategoria() {
     $('#AddCat').modal('show');
};

async function cadastrar_categoria() {
    if ($('#nome_cat').val() != "" && $('#descricao_cat').val() != "") {
        var titulo = $('#nome_cat').val();
        var descricao = $('#descricao_cat').val();
        var imagem = $('#img_cat').val();

        await axios({
            //tras todos as plenarias no option select
            method: 'POST',
            url: 'http://localhost:3003/categorias',
            headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
            data: {
                titulo,
                descricao,
                imagem
            }
        }).then(function (response) {
                // location.reload();
                $('#AddCat').modal('hide');
                $('#confirmacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(function (response) {
                //console.log(error);
                $('#AddCat').modal('hide');
                $('#negacaoCadastro').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            });
    } else {
        alert("Campos incompletos");
    }
};

async function edit(id) {
    $('#EditCat').modal('show');
    const {data, status} = await axios({
        method:"GET",
        url: `http://localhost:3003/categorias/${id}`,
        headers: {'Authorization': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
    });
        if(status == 200){
            console.log(data.titulo);
                $('#id_cat_edit').val(data.id);
                $('#nome_cat_edit').val(data.titulo);
                $('#descricao_cat_edit').val(data.descricao)
                //$('#img_cat_edit').val(value.imagem);
                document.getElementById('show-img-edit').setAttribute('src',data.imagem);
        }
}

async function editar_categoria(id) {
    if ($('#nome_cat_edit').val() != "") {
        var id = $('#id_cat_edit').val();
        var titulo = $('#nome_cat_edit').val();
        var descricao = $('#descricao_cat_edit').val();
        var imagem = $('#img_cat_edit').val();

        // if(imagem==""){
        //     base64 = document.getElementById('show-img-edit').src;
        // }

        await axios({
            url: `http://localhost:3003/categorias/${id}`, 
            method: "PUT",
            headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'},
            data: {
                titulo,
                descricao,
                imagem
            }
        }).then(response => {
                $('#EditCat').modal('hide');
                $('#confirmacaoEditar').modal('show');
                setTimeout(
                    function () {
                        location.reload();
                    }, 2000
                );
            })
            .catch(error => {
                console.log(error);
            });
    } else {
        alert("Campos incompletos");
    }
}

function delCategoria(id) {
    console.log("teste")
    $('#DelCat').modal('show');
};

async function excluir_categoria(id){
    const response = await axios({
        url: `http://localhost:3003/categorias/${id}`,
        method : "DELETE",
        headers: {'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwiaWF0IjoxNjI3NDc4OTk3LCJleHAiOjE2Mjc1MTQ5OTd9.qYD5MaZV-HftW0kYHSuIoJ8n_CYUmWhc9HVPihAR530'}
    })
    if(response.status == 202){
        $('#confirmacaoExcluir').modal('show');
        setTimeout(
            function () {
                location.reload();
            }, 2000
        );
    }else{
        console.log(response);
    }
}
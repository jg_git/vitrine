var selectize_array = [];
	var base64_prod_1;
	var base64_prod_2;
	var base64_prod_3;
	var base64_prod_4;

	$(function () {
		$('#subcat_prod').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: true,
			create: false
		});
	});

	$(document).ready(function () {
		$('#falseinput_1').click(function () {
			$("#img_prod_1").click();
		});
		$('#falseinput_2').click(function () {
			$("#img_prod_2").click();
		});
		$('#falseinput_3').click(function () {
			$("#img_prod_3").click();
		});
		$('#falseinput_4').click(function () {
			$("#img_prod_4").click();
		});
		$('#falseinput_1_edit').click(function () {
			$("#img_prod_1_edit").click();
		});
		$('#falseinput_2_edit').click(function () {
			$("#img_prod_2_edit").click();
		});
		$('#falseinput_3_edit').click(function () {
			$("#img_prod_3_edit").click();
		});
		$('#falseinput_4_edit').click(function () {
			$("#img_prod_4_edit").click();
		});
	});

	$('#img_prod_1').change(function () {
		$('#selected_filename_1').html($('#img_prod_1')[0].files[0].name);
		$('#div-img-1').removeClass("hide").addClass("show");
	});


	$('#img_prod_2').change(function () {
		$('#selected_filename_2').html($('#img_prod_2')[0].files[0].name);
		$('#div-img-2').removeClass("hide").addClass("show");
	});

	$('#img_prod_3').change(function () {
		$('#selected_filename_3').html($('#img_prod_3')[0].files[0].name);
		$('#div-img-3').removeClass("hide").addClass("show");
	});

	$('#img_prod_4').change(function () {
		$('#selected_filename_4').html($('#img_prod_4')[0].files[0].name);
		$('#div-img-4').removeClass("hide").addClass("show");
	});

	$(function () {
		$('#subcat_prod_edit').selectize({
			plugins: ['remove_button'],
			delimiter: ',',
			persist: true,
			create: false
		});
	});

	$('#img_prod_1_edit').change(function () {
		$('#selected_filename_1_edit').html($('#img_prod_1_edit')[0].files[0].name);
		$('#div-img-edit-1').removeClass("hide").addClass("show");
	});


	$('#img_prod_2_edit').change(function () {
		$('#selected_filename_2_edit').html($('#img_prod_2_edit')[0].files[0].name);
		$('#div-img-edit-2').removeClass("hide").addClass("show");
	});

	$('#img_prod_3_edit').change(function () {
		$('#selected_filename_3_edit').html($('#img_prod_3_edit')[0].files[0].name);
		$('#div-img-edit-3').removeClass("hide").addClass("show");
	});

	$('#img_prod_4_edit').change(function () {
		$('#selected_filename_4_edit').html($('#img_prod_4_edit')[0].files[0].name);
		$('#div-img-edit-4').removeClass("hide").addClass("show");
	});

	$(document).ready(function () {
		var aux_marca = 0;
		var select_marca = document.getElementById('marca_prod_edit');
		select_marca.innerText = "";
		axios.get('http://localhost:3000/marcas').then(resp => {
			resp.data.forEach(function (value) {
				if (aux_marca == 0) {
					var z = document.createElement("option");
					var y = document.createTextNode("Selecione a marca");
					z.appendChild(y);
					select_marca.appendChild(z);
					aux_marca++;
				}

				var marca = document.createElement("option");
				marca.setAttribute("value", value.id);
				var marca_nome = document.createTextNode(value.nome);
				marca.appendChild(marca_nome);

				select_marca.appendChild(marca);
			});
		});

		var aux = 0;
		var select_categoria = document.getElementById('cat_prod_edit');
		select_categoria.innerText = "";
		axios.get('http://localhost:3000/subcategoriasCatRef').then(resp => {
			resp.data.forEach(function (value) {
				if (aux == 0) {
					var z = document.createElement("option");
					var y = document.createTextNode("Selecione a categoria");
					z.appendChild(y);
					select_categoria.appendChild(z);
					aux++;
				}

				var x = document.createElement("option");
				x.setAttribute("value", value.id);
				var t = document.createTextNode(value.nome);
				x.appendChild(t);

				select_categoria.appendChild(x);
			});
		});
	});

	function mostrarimg(obj, edit) {
		var file = document.getElementById(obj.id).files;
		var n = obj.id;
		n = n.split("_");
		console.log(n[2]);

		if (file.length > 0) {
			var reader = new FileReader();

			reader.onload = function (e) {
				if (n[2] == 1) {
					base64_prod_1 = e.target.result;
				} else if (n[2] == 2) {
					base64_prod_2 = e.target.result;
				} else if (n[2] == 3) {
					base64_prod_3 = e.target.result;
				} else if (n[2] == 4) {
					base64_prod_4 = e.target.result;
				}

				if (edit == 0) {
					document.getElementById('show-img-' + n[2]).setAttribute("src", e.target.result);
				} else if (edit == 1) {
					document.getElementById('show-img-edit-' + n[2]).setAttribute("src", e.target.result);
				}
			}

			reader.readAsDataURL(file[0]);
		}
	}

	$(document).ready(function () {
		$("#buscaCep").click(function () {

			//Nova variável "cep" somente com dígitos.
			var cep = $("#cep_for").val().replace(/\D/g, '');

			//Verifica se campo cep possui valor informado.
			if (cep != "") {

				//Expressão regular para validar o CEP.
				var validacep = /^[0-9]{8}$/;

				//Valida o formato do CEP.
				if (validacep.test(cep)) {

					//Consulta o webservice viacep.com.br/
					$.getJSON("//viacep.com.br/ws/" + cep + "/json/?callback=?", function (dados) {

						if (!("erro" in dados)) {
							//Atualiza os campos com os valores da consulta.
							$("#endereco_for").val(dados.logradouro);
							$("#endereco_for").css("background", "#eee");
							$("#bairro_for").val(dados.bairro);
							$("#bairro_for").css("background", "#eee");
							$("#cidade_for").val(dados.localidade);
							$("#cidade_for").css("background", "#eee");
						} //end if.
						else {
							//CEP pesquisado não foi encontrado.
							alert("CEP não encontrado.");
						}
					});
				} //end if.
				else {
					alert("Formato de CEP inválido.");
				}
			} //end if.
		});
	});

	axios.get('http://localhost:3000/produtos').then(resp => {
		var tbody = document.getElementById('conteudo-produtos');
		resp.data.forEach(function (value) {
			var tr = document.createElement('tr');
			var id = value.id;
			var td_nome = document.createElement('td');
			var td_tamanho = document.createElement('td');
			var td_genero = document.createElement('td');
			var td_quantidade = document.createElement('td');
			var td_descricao = document.createElement('td');
			var td_marca = document.createElement('td');
			var td_editar = document.createElement('td');

			td_nome.innerHTML = value.nome;
			td_tamanho.innerHTML = value.tamanho;
			td_genero.innerHTML = value.genero;
			td_quantidade.innerHTML = value.quantidade;
			td_descricao.innerHTML = value.descricao;
			td_marca.innerHTML = value.marca;

			td_editar.innerHTML =
				"<center>" +
				"<button class='btn btn-warning btn-circle' onclick='edit(" + id + ")' >" +
				"<i class='fas fa-edit' ></i>" +
				"</button>" +
				"</center>";

			tr.appendChild(td_nome);
			tr.appendChild(td_tamanho);
			tr.appendChild(td_genero);
			tr.appendChild(td_quantidade);
			tr.appendChild(td_descricao);
			tr.appendChild(td_marca);
			tr.appendChild(td_editar);

			tbody.appendChild(tr);
		});

		$(document).ready(function () {
			$('#dataTable').DataTable({});
		});
	});

	function addProduto() {
		$('#AddProd').modal('show');

		$('#AddProd').on('shown.bs.modal', function () {
			$('#cod_prod').trigger('focus');
		});

		var aux_marca = 0;
		var select_marca = document.getElementById('marca_prod');
		select_marca.innerText = "";
		axios.get('http://localhost:3000/marcas').then(resp => {
			resp.data.forEach(function (value) {
				if (aux_marca == 0) {
					var z = document.createElement("option");
					var y = document.createTextNode("Selecione a marca");
					z.appendChild(y);
					select_marca.appendChild(z);
					aux_marca++;
				}

				var marca = document.createElement("option");
				marca.setAttribute("value", value.id);
				var marca_nome = document.createTextNode(value.nome);
				marca.appendChild(marca_nome);

				select_marca.appendChild(marca);
			});
		});

		var aux = 0;
		var select = document.getElementById('cat_prod');
		select.innerText = "";
		axios.get('http://localhost:3000/subcategoriasCatRef').then(resp => {
			resp.data.forEach(function (value) {
				if (aux == 0) {
					var z = document.createElement("option");
					var y = document.createTextNode("Selecione a categoria");
					z.appendChild(y);
					select.appendChild(z);
					aux++;
				}

				var x = document.createElement("option");
				x.setAttribute("value", value.id);
				var t = document.createTextNode(value.nome);
				x.appendChild(t);

				select.appendChild(x);
			});
		});
	};

	var cod_barras;
	var ref;
	var nome;
	var marca;
	var genero;
	var descricao;
	var qtd;
	var tamanho;
	var tipo_tamanho;
	var categoria;
	var subcategoria;
	var valor_compra;
	var valor_venda;
	var img_1;
	var img_2;
	var img_3;
	var img_4;

	async function cadastrar_produto(aux) {
		if ($('#cod_prod').val() != "" &&
			$('#nome_prod').val() != "" && $('#marca_prod').val() != "" &&
			$('#genero_prod').val() != "" && $('#descricao_prod').val() != "" &&
			$('#qtd_prod').val() != "" && $('#tam_prod').val() != "" &&
			$('#cat_prod').val() != "" && $('#subcat_prod').val() != "" &&
			$('#valor_compra_prod').val() != "" && $('#valor_venda_prod').val() != "" &&
			($('#img_prod_1').val() != "" || $('#img_prod_2').val() != "" ||
				$('#img_prod_3').val() != "" || $('#img_prod_4').val() != "")) {

			//DENTRO DO IF DE VERIFICAÇÃO
			cod_barras = $('#cod_prod').val();

			if ($('#ref_prod').val()) {
				ref = $('#ref_prod').val();
			}

			nome = $('#nome_prod').val();
			marca = $('#marca_prod').val();
			genero = $('#genero_prod').val();
			descricao = $('#descricao_prod').val();
			qtd = $('#qtd_prod').val();
			tipo_tamanho = $('#tipo_tam_prod').val();
			tamanho = $('#tam_prod').val();
			categoria = $('#cat_prod').val();
			subcategoria = $('#subcat_prod').val();
			console.log(subcategoria);
			valor_compra = $('#valor_compra_prod').val();
			valor_venda = $('#valor_venda_prod').val();

			if ($('#img_prod_1').val() != "") {
				img_1 = $('#img_prod_1').val();
			}

			if ($('#img_prod_2').val() != "") {
				img_2 = $('#img_prod_2').val();
			}

			if ($('#img_prod_3').val() != "") {
				img_3 = $('#img_prod_3').val();
			}

			if ($('#img_prod_4').val() != "") {
				img_4 = $('#img_prod_4').val();
			}

			await axios({
				//tras todos as plenarias no option select
				method: 'POST',
				url: 'http://localhost:3000/produtosCreate',
				data: {
					cod_barras,
					ref,
					nome,
					tipo_tamanho,
					tamanho,
					genero,
					quantidade: qtd,
					valor_compra,
					valor_venda,
					descricao,
					marca,
					categoria,
					subcategoria,
					img_1: base64_prod_1,
					img_2: base64_prod_2,
					img_3: base64_prod_3,
					img_4: base64_prod_4
				},
				headers: {}
			})
				.then(function (response) {
					if (aux == 0) {
						$('#AddProd').modal('hide');
						$('#confirmacaoCadastro').modal('show');
						setTimeout(
							function () {
								location.reload();
							}, 2000
						);
					} else if (aux == 1) {
						$('#AddProd').modal('hide');
						$('#confirmacaoCadastro').modal('show');

						setTimeout(
							function () {
								$('#confirmacaoCadastro').modal('hide');
								$('#AddProd').modal('show');
								$('#cod_prod').val("");
								$('#ref_prod').val("");
								$('#nome_prod').val(nome);
								$('#marca_prod').val(marca);
								$('#genero_prod').val(genero);
								$('#descricao_prod').val(descricao);
								$('#qtd_prod').val(qtd);
								$('#tam_prod').val(tamanho);
								$('#cat_prod').val(categoria);
								$('#subcat_prod').val(subcategoria);
								$('#valor_compra_prod').val(valor_compra);
								$('#valor_venda_prod').val(valor_venda)
							}, 2000
						);
					}

				})
				.catch(function (response) {
					console.log(error);
					$('#AddProd').modal('hide');
					$('#negacaoCadastro').modal('show');
					setTimeout(
						function () {
							$('#AddProd').modal('show');
						}, 2000
					);
				});
		} else {
			alert("Campos incompletos");
		}
	};

	function edit(id) {
		$('#EditProd').modal('show');

		axios.get('http://localhost:3000/produtosGetEdit?id=' + id).then(resp => {
			resp.data.forEach(function (value) {
				$('#id_prod_edit').val(value.id);
				$('#cod_prod_edit').val(value.cod_barras);
				$('#ref_prod_edit').val(value.ref);
				$('#nome_prod_edit').val(value.nome);
				$('#marca_prod_edit').val(value.marca);
				$('#genero_prod_edit').val(value.genero);
				$('#cat_prod_edit').val(value.categoria);
				$('#descricao_prod_edit').val(value.descricao);
				$('#qtd_prod_edit').val(value.quantidade);
				$('#tipo_tam_prod_edit').val(value.tipo_tamanho);

				var select_tamanho = document.getElementById('tam_prod_edit');
				select_tamanho.innerText = "";
				axios.get('http://localhost:3000/produtosTamanhoConsulta?id=' + value.tipo_tamanho).then(resp => {
					resp.data.forEach(function (value_tam) {
						var tamopt = document.createElement("option");
						tamopt.setAttribute("value", value_tam.id);
						var tam = document.createTextNode(value_tam.tamanho);
						tamopt.appendChild(tam);

						select_tamanho.appendChild(tamopt);
						console.log(tamopt);
					});
					$('#tam_prod_edit').val(value.tamanho);
				});

				$('#cat_prod_edit').val(value.categoria);

				var select_subcat = document.getElementById("subcat_prod_edit");
				var select_subcat2 = $('#subcat_prod_edit').selectize();

				if ($('#cat_prod_edit').val() != "") {
					axios.get('http://localhost:3000/subcategoriasSubRef?id=' + value.categoria).then(resp => {
						for (var i = 0; i < selectize_array.length; i++) {
							select_subcat2[0].selectize.removeOption(selectize_array[i]);
						}

						resp.data.forEach(function (value_subcat) {
							selectize_array.push(value_subcat.id);
							select_subcat.selectize.addOption({ value: value_subcat.id, text: value_subcat.nome });
						});

						select_subcat.selectize.setValue(value.subcategoria_id);
					});
				}

				$('#valor_compra_prod_edit').val(value.valor_compra);
				$('#valor_venda_prod_edit').val(value.valor_venda);

				document.getElementById('show-img-edit-1').setAttribute('src', value.img_1);
				document.getElementById('show-img-edit-2').setAttribute('src', value.img_2);
				document.getElementById('show-img-edit-3').setAttribute('src', value.img_3);
				document.getElementById('show-img-edit-4').setAttribute('src', value.img_4);
			});
		});
		aux = 0;
	}

	function editar_produto() {

		if ($('#cod_prod_edit').val() != "" &&
			$('#nome_prod_edit').val() != "" && $('#marca_prod_edit').val() != "" &&
			$('#genero_prod_edit').val() != "" && $('#descricao_prod_edit').val() != "" &&
			$('#qtd_prod_edit').val() != "" && $('#tam_prod_edit').val() != "" &&
			$('#cat_prod_edit').val() != "" && $('#subcat_prod_edit').val() != "" &&
			$('#valor_compra_prod_edit').val() != "" && $('#valor_venda_prod_edit').val() != "" &&
			$('#tipo_tam_prod_edit').val() != "") {


			var id_edit = $('#id_prod_edit').val();
			var cod_barras_edit = $('#cod_prod_edit').val();
			var ref_edit = $('#ref_prod_edit').val();
			var nome_edit = $('#nome_prod_edit').val();
			var marca_edit = $('#marca_prod_edit').val();
			var genero_edit = $('#genero_prod_edit').val();
			var descricao_edit = $('#descricao_prod_edit').val();
			var qtd_edit = $('#qtd_prod_edit').val();
			var tamanho_edit = $('#tam_prod_edit').val();
			var tipo_tamanho_edit = $('#tipo_tam_prod_edit').val();
			var categoria_edit = $('#cat_prod_edit').val();
			var subcategoria_edit = $('#subcat_prod_edit').val();
			var valor_compra_edit = $('#valor_compra_prod_edit').val();
			var valor_venda_edit = $('#valor_venda_prod_edit').val();
			var img_1_edit = $('#img_prod_1_edit').val();
			var img_2_edit = $('#img_prod_2_edit').val();
			var img_3_edit = $('#img_prod_3_edit').val();
			var img_4_edit = $('#img_prod_4_edit').val();

			if (img_1_edit == "") {
				base64_prod_1 = document.getElementById('show-img-edit-1').src;
			}

			if (img_2_edit == "") {
				base64_prod_2 = document.getElementById('show-img-edit-2').src;
			}

			if (img_3_edit == "") {
				base64_prod_3 = document.getElementById('show-img-edit-3').src;
			}

			if (img_4_edit == "") {
				base64_prod_4 = document.getElementById('show-img-edit-4').src;
			}

			axios.put('http://localhost:3000/produtosEdit?id=' + id_edit, {
				cod_barras: cod_barras_edit,
				ref: ref_edit,
				nome: nome_edit,
				tipo_tamanho: tipo_tamanho_edit,
				tamanho: tamanho_edit,
				genero: genero_edit,
				quantidade: qtd_edit,
				valor_compra: valor_compra_edit,
				valor_venda: valor_venda_edit,
				descricao: descricao_edit,
				marca: marca_edit,
				categoria: categoria_edit,
				subcategoria: subcategoria_edit,
				img_1: base64_prod_1,
				img_2: base64_prod_2,
				img_3: base64_prod_3,
				img_4: base64_prod_4
			})
				.then(response => {
					console.log(response);
					$('#EditProd').modal('hide');
					$('#confirmacaoEditar').modal('show');
					setTimeout(
						function () {
							location.reload();
						}, 2000
					);
				})
				.catch(error => {
					console.log(error);
				});
		} else {
			alert("Campos incompletos");
		}
	}

	function defineTamanho(obj, edit) {
		if (edit == 0) {
			var select = document.getElementById("tam_prod");
		} else if (edit == 1) {
			var select = document.getElementById("tam_prod_edit");
		}
		select.innerText = "";
		if (obj.value != "") {
			axios.get('http://localhost:3000/produtosTamanhoConsulta?id=' + obj.value).then(resp => {
				resp.data.forEach(function (value) {
					var x = document.createElement("option");
					x.setAttribute("value", value.id);
					var t = document.createTextNode(value.tamanho);
					x.appendChild(t);

					select.appendChild(x);
					select.removeAttribute("readonly");
					console.log(x);
				});
			});
		} else {
			select.setAttribute("readonly", "readonly");
		}
	}

	function encontrarSubcategorias(obj, edit) {
		if (edit == 0) {
			var select = document.getElementById("subcat_prod");
			var select2 = $('#subcat_prod').selectize();
		} else if (edit == 1) {
			var select = document.getElementById("subcat_prod_edit");
			var select2 = $('#subcat_prod_edit').selectize();
		}

		if (obj.value != "") {
			axios.get('http://localhost:3000/subcategoriasSubRef?id=' + obj.value).then(resp => {
				for (var i = 0; i < selectize_array.length; i++) {
					select2[0].selectize.removeOption(selectize_array[i]);
				}

				resp.data.forEach(function (value) {
					selectize_array.push(value.id);
					select.selectize.addOption({ value: value.id, text: value.nome });
				});
			});
		}
	}
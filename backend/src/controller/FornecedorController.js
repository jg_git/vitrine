const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('*').from('fornecedor');
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { nome, cnpj, cep, endereco, numero, bairro, cidade, telefone } = req.body;
        try {
            await knex('fornecedor').insert({
                nome,
                cnpj,
                cep,
                endereco,
                numero,
                bairro,
                cidade,
                telefone
            });
            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('*').from('fornecedor').where('id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { nome, cnpj, cep, endereco, numero, bairro, cidade, telefone } = req.body;
        try {
            const response = await knex('fornecedor')
                .where('id', id)
                .update({
                    nome,
                    cnpj,
                    cep,
                    endereco,
                    numero,
                    bairro,
                    cidade,
                    telefone
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }




}
const knex = require('../database');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('*').from('categoria');
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { nome, img } = req.body;
        try {
            await knex('categoria').insert({
                nome,
                img
            });
            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('*').from('categoria').where('id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { nome, img } = req.body;
        try {
            const response = await knex('categoria')
                .where('id', id)
                .update({
                    nome,
                    img
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }




}
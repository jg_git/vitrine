const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async getedit(req, res) {
        try {
            const response = await knex.select('*').from('empresa');
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },
  
    async edit(req, res) {
        const { razao_social, nome_fantasia, email, cnpj, ie, telefone, endereco, numero, bairro, cidade, img_1, img_2, img_3} = req.body;
        try {
            const response = await knex('empresa')
                .update({
                    razao_social,
                    nome_fantasia,
                    email,
                    cnpj,
                    ie,
                    telefone,
                    endereco,
                    numero,
                    bairro,
                    cidade,
                    logo_principal_g: img_1,
                    logo_principal_p: img_2,
                    logo_cupom: img_3
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }
}
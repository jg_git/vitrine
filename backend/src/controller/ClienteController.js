const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('*').from('cliente');
            for (var i = 0; i < response.length; i++) {
                response[i]['data_nasc'] = moment(response[i]['data_nasc']).format('DD/MM/YYYY');
            }
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { nome, data_nasc, cpf, rg, email, cep, endereco,
            numero, bairro, cidade, telefone, observacao, limite_venda } = req.body;
        try {
            await knex('cliente').insert({
                nome, data_nasc, cpf, rg, email, cep, endereco, numero,
                bairro, cidade, telefone, observacao, limite_venda
            });
            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('*').from('cliente').where('id', id);
            for(var i = 0 ; i<response.length; i++){
                response[i]['data_nasc'] = moment(response[i]['data_nasc']).format('YYYY-MM-DD');
            }
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { nome, data_nasc, cpf, rg, email, cep, endereco,
            numero, bairro, cidade, telefone, observacao, limite_venda } = req.body;
        try {
            const response = await knex('cliente')
                .where('id', id)
                .update({
                    nome,
                    data_nasc,
                    cpf,
                    rg,
                    email,
                    cep,
                    endereco,
                    numero, 
                    bairro, 
                    cidade, 
                    telefone, 
                    observacao, 
                    limite_venda
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }




}
const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('s.id','s.nome',{categoria:'c.nome'}).from({s:'subcategoria'})
            .innerJoin({c:'categoria'},'s.id_categoria','c.id');
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { nome,id_categoria,img } = req.body;
        try {
            await knex('subcategoria').insert({
                nome,
                id_categoria,
                img
            });
            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('*').from('subcategoria').where('id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { nome,id_categoria,img } = req.body;
        try {
            const response = await knex('subcategoria')
                .where('id', id)
                .update({
                    nome,
                    id_categoria,
                    img
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async getCategoria(req,res){
        try{
            const response = await knex.select('*').from('categoria');
            return res.json(response);
        }catch(error){
            return res.json(error);
        }
    },

    async getSubcategoria(req,res){
        const id = req.query.id;
        try{
            const response = await knex.select('*').from('subcategoria').where('id_categoria',id);
            return res.json(response);
        }catch(error){
            return res.json(error);
        }
    },

}
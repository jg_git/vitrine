const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('c.id', 'c.data_saida', 'c.data_volta', 'cli.nome as cliente').sum('cp.valor as valor_condicional')
                .from('condicional as c').innerJoin('condicional_prod as cp', 'c.id', 'cp.id_condicional')
                .innerJoin('cliente as cli', 'c.id_cliente', 'cli.id').innerJoin('produto as p', 'cp.id_produto', 'p.id').groupBy('c.id');

            for (var i = 0; i < response.length; i++) {
                response[i]['data_saida'] = moment(response[i]['data_saida']).format('DD/MM/YYYY');

                if (response[i]['data_volta'] != null) {
                    response[i]['data_volta'] = moment(response[i]['data_volta']).format('DD/MM/YYYY');
                } else {
                    response[i]['data_volta'] = "Sem confirmação de volta";
                }
            }
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { cliente, count_prod, produtos, qtds, data_saida } = req.body;
        try {
            const id_condicional = await knex('condicional').insert({
                id_cliente: cliente,
                data_saida
            }).returning('id');

            for (var i = 0; i < count_prod; i++) {
                //Atribuindo o valor aos itens
                var prod_estoque = await knex.select('valor_venda', 'quantidade').from('produto').where('id', produtos[i]);
                var valor_tot = qtds[i] * prod_estoque[0]['valor_venda'];

                //Insert dos itens em condicional_prod
                await knex('condicional_prod').insert({
                    id_condicional,
                    id_produto: produtos[i],
                    quantidade: qtds[i],
                    valor: valor_tot
                });

                //Retirando a quantidade da tabela produtos
                var qtd_estoque = prod_estoque[0]['quantidade'];
                var dif_qtds = qtd_estoque - qtds[i];
                if (dif_qtds == 0) {
                    await knex('produto')
                        .where('id', produtos[i])
                        .update({
                            quantidade: dif_qtds,
                            status: 1
                        });
                } else {
                    await knex('produto')
                        .where('id', produtos[i])
                        .update({
                            quantidade: dif_qtds
                        });
                }

            }

            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('tamanho.id', 'tipo_tamanho.tipo', 'tamanho.id_tipo_tamanho', 'tamanho.tamanho').from('tamanho')
                .innerJoin('tipo_tamanho', 'tamanho.id_tipo_tamanho', 'tipo_tamanho.id')
                .where('tamanho.id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { tamanho, tipo } = req.body;
        try {
            const response = await knex('tamanho')
                .where('id', id)
                .update({
                    tamanho,
                    id_tipo_tamanho: tipo
                });
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }




}
const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('*').from('marca');
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { nome,img } = req.body;
        try {
            await knex('marca').insert({
                nome,
                img
            });
            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('*').from('marca').where('id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { nome,img } = req.body;
        try {
            const response = await knex('marca')
                .where('id', id)
                .update({
                    nome,
                    img
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }




}
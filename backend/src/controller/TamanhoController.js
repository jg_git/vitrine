const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('tamanho.id','tipo_tamanho.tipo','tamanho.tamanho').from('tamanho')
            .innerJoin('tipo_tamanho','tamanho.id_tipo_tamanho','tipo_tamanho.id');
            console.log(response);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { tamanho,tipo } = req.body;
        try {
            await knex('tamanho').insert({
                tamanho,
                id_tipo_tamanho: tipo
            });
            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('tamanho.id','tipo_tamanho.tipo','tamanho.id_tipo_tamanho','tamanho.tamanho').from('tamanho')
            .innerJoin('tipo_tamanho','tamanho.id_tipo_tamanho','tipo_tamanho.id')
            .where('tamanho.id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { tamanho,tipo } = req.body;
        try {
            const response = await knex('tamanho')
                .where('id', id)
                .update({
                    tamanho,
                    id_tipo_tamanho: tipo
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }




}
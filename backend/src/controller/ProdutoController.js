const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {
    async consulta(req, res) {
        try {
            const response = await knex.select('p.id', 'p.nome', 't.tamanho', 'p.genero', 'p.quantidade', 'p.descricao', 'm.nome as marca').from({ p: 'produto' })
                .innerJoin({ t: 'tamanho' }, 'p.tamanho', 't.id').innerJoin('marca as m', 'p.marca', 'm.id');
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async consultaSelect(req, res) {
        try {
            const response = await knex.select('p.id', 'p.nome', 't.tamanho', 'p.genero', 'p.quantidade', 'p.descricao', 'm.nome as marca').from({ p: 'produto' })
                .innerJoin({ t: 'tamanho' }, 'p.tamanho', 't.id').innerJoin('marca as m', 'p.marca', 'm.id').where('p.status', 0);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async tamconsulta(req, res) {
        try {
            const id = req.query.id;
            if (id == 3) {
                const response = await knex.select('*').from('tamanho')
                    .orderBy('tamanho', 'desc');
                return res.json(response);
            } else {
                const response = await knex.select('*').from('tamanho')
                    .where('id_tipo_tamanho', id)
                    .orderBy('tamanho', 'desc');
                return res.json(response);
            }
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { cod_barras, ref, nome, tipo_tamanho, tamanho, genero, quantidade, valor_compra,
            valor_venda, descricao, marca, categoria, subcategoria, img_1, img_2,
            img_3, img_4 } = req.body;
        try {
            const produto_id = await knex('produto').insert({
                cod_barras,
                ref,
                nome,
                tipo_tamanho,
                tamanho,
                genero,
                quantidade,
                valor_compra,
                valor_venda,
                descricao,
                marca,
                categoria,
                img_1,
                img_2,
                img_3,
                img_4
            }).returning('id');

            for (var i = 0; i < subcategoria.length; i++) {
                await knex('produto_subcategoria').insert({
                    id_produto: produto_id,
                    id_subcategoria: subcategoria[i]
                });
            }

            console.log(produto_id);

            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('*').from('produto as p').where('p.id', id);
            const subcategorias = await knex.select('*').from('produto_subcategoria as ps')
                .where('ps.id_produto', response[0].id);

            const subcategorias_id = subcategorias.map((id) => {
                return id.id_subcategoria;
            });

            response[0]['subcategoria_id'] = subcategorias_id;
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async getpdv(req, res) {
        const cod = req.query.cod;
        try {
            const response = await knex.select('p.id','p.nome','t.tamanho','p.genero','p.quantidade','p.valor_venda','m.nome as marca','p.img_1').from('produto as p')
            .innerJoin('tamanho as t','p.tamanho','t.id')
            .innerJoin('marca as m','p.marca','m.id')
            .where('p.cod_barras', cod);

            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { cod_barras, ref, nome, tipo_tamanho, tamanho, genero, quantidade, valor_compra,
            valor_venda, descricao, marca, categoria, subcategoria, img_1, img_2,
            img_3, img_4 } = req.body;
        try {
            const response = await knex('produto')
                .where('id', id)
                .update({
                    cod_barras,
                    ref,
                    nome,
                    tipo_tamanho,
                    tamanho,
                    genero,
                    quantidade,
                    valor_compra,
                    valor_venda,
                    descricao,
                    marca,
                    categoria,
                    img_1,
                    img_2,
                    img_3,
                    img_4
                });

            await knex('produto_subcategoria').where('id_produto', id).del();

            for (var i = 0; i < subcategoria.length; i++) {
                await knex('produto_subcategoria').insert({
                    id_produto: id,
                    id_subcategoria: subcategoria[i]
                });
            }

            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async getqtd(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('quantidade').from('produto').where('id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }




}
const knex = require('../database');
const moment = require('moment');
//get - consulta
//create - insert
//update
//delete  
module.exports = {

    async consulta(req, res) {
        try {
            const response = await knex.select('*').from('pagamento').orderBy([{ column: 'tipo' }, { column: 'id' }]);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async inserir(req, res) {
        const { nome, tipo, taxa, qtd_vezes } = req.body;
        try {
            await knex('pagamento').insert({
                nome,
                qtd_vezes,
                taxa,
                tipo
            });
            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async getedit(req, res) {
        const id = req.query.id;
        try {
            const response = await knex.select('*').from('pagamento').where('id', id);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async edit(req, res) {
        const id = req.query.id;
        const { nome, tipo, taxa, qtd_vezes } = req.body;
        try {
            const response = await knex('pagamento')
                .where('id', id)
                .update({
                    nome,
                    qtd_vezes,
                    taxa,
                    tipo
                })
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    },

    async pdvselect(req, res) {
        const tipo = req.query.tipo;
        try {
            const response = await knex.select('*').from('pagamento').where('tipo', tipo);
            return res.json(response);
        } catch (error) {
            return res.json(error);
        }
    }

}
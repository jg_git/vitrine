const knex = require('../database');
//get - consulta
//create - insert
//update
//delete  
module.exports = {
    async inserir(req, res) {
        const { cliente, prods, qtds, precos, formas_pag, valores_pag, desconto, troco } = req.body;
        try {
            const id_venda = await knex('venda').insert({
                cliente,
                desconto
            }).returning('id');

            for (var i = 0; i < prods.length; i++) {
                await knex('venda_prod').insert({
                    id_venda,
                    produto: prods[i],
                    qtd: qtds[i],
                    preco: precos[i]
                });
            }

            for (var i = 0; i < formas_pag.length; i++) {
                await knex('venda_pagamento').insert({
                    id_venda,
                    forma_pagamento: formas_pag[i],
                    valor: valores_pag[i]
                });
            }

            return res.send("Cadastrado com sucesso!");
        } catch (error) {
            return res.json(error);
        }
    },

    async statusCaixa(req, res) {
        const id_caixa = req.query.id_caixa;
        try {

            if (id_caixa) {
                const response =
                    await knex.select('*').from('caixa')
                        .where('status', 1)
                        .where('id', id_caixa);
                return res.json(response);
            } else {
                const response =
                    await knex.select('*').from('caixa')
                        .where('status', 1);
                return res.json(response);
            }

        } catch (error) {
            return res.json(error);
        }
    },

    async abrirCaixa(req, res) {
        const { valor_caixa } = req.body;
        try {
            const id_caixa = await knex('caixa').insert({
                valor_caixa,
                data_abertura: knex.fn.now(),
                status: 1
            }).returning('id');

            return res.json(id_caixa);
        } catch (error) {
            return res.json(error);
        }
    }


}
const express = require('express');
const routes = express.Router();

const ClienteController = require('./controller/ClienteController');
routes.get('/clientes', ClienteController.consulta);
routes.post('/clientesCreate', ClienteController.inserir);
routes.get('/clientesGetEdit', ClienteController.getedit);
routes.put('/clientesEdit', ClienteController.edit);

const FornecedorController = require('./controller/FornecedorController');
routes.get('/fornecedores', FornecedorController.consulta);
routes.post('/fornecedoresCreate', FornecedorController.inserir);
routes.get('/fornecedoresGetEdit', FornecedorController.getedit);
routes.put('/fornecedoresEdit', FornecedorController.edit);

const TamanhoController = require('./controller/TamanhoController');
routes.get('/tamanhos', TamanhoController.consulta);
routes.post('/tamanhosCreate', TamanhoController.inserir);
routes.get('/tamanhosGetEdit', TamanhoController.getedit);
routes.put('/tamanhosEdit', TamanhoController.edit);

const ProdutoController = require('./controller/ProdutoController');
routes.get('/produtos', ProdutoController.consulta);
routes.get('/produtosSelect', ProdutoController.consultaSelect);
routes.get('/produtosTamanhoConsulta',ProdutoController.tamconsulta);
routes.post('/produtosCreate', ProdutoController.inserir);
routes.get('/produtosGetEdit', ProdutoController.getedit);
routes.get('/produtosGetPdv', ProdutoController.getpdv);
routes.put('/produtosEdit', ProdutoController.edit);
routes.get('/produtosGetQtd', ProdutoController.getqtd);

const categoriaController = require('./controller/CategoriaController');
routes.get('/categorias', categoriaController.consulta);
routes.post('/categoriasCreate', categoriaController.inserir);
routes.get('/categoriasGetEdit', categoriaController.getedit);
routes.put('/categoriasEdit', categoriaController.edit);

const subcategoriaController = require('./controller/SubcategoriaController');
routes.get('/subcategorias', subcategoriaController.consulta);
routes.post('/subcategoriasCreate', subcategoriaController.inserir);
routes.get('/subcategoriasGetEdit', subcategoriaController.getedit);
routes.put('/subcategoriasEdit', subcategoriaController.edit);
routes.get('/subcategoriasCatRef', subcategoriaController.getCategoria);
routes.get('/subcategoriasSubRef', subcategoriaController.getSubcategoria);

const marcaController = require('./controller/marcaController');
routes.get('/marcas', marcaController.consulta);
routes.post('/marcasCreate', marcaController.inserir);
routes.get('/marcasGetEdit', marcaController.getedit);
routes.put('/marcasEdit', marcaController.edit);

const CondicionalController = require('./controller/CondicionalController');
routes.get('/condicionais', CondicionalController.consulta);
routes.post('/condicionaisCreate', CondicionalController.inserir);
// routes.get('/condicionaisGetEdit', CondicionalController.getedit);
// routes.put('/condicionaisEdit', CondicionalController.edit);

const pagamentoController = require('./controller/PagamentoController');
routes.get('/pagamentos', pagamentoController.consulta);
routes.post('/pagamentosCreate', pagamentoController.inserir);
routes.get('/pagamentosGetEdit', pagamentoController.getedit);
routes.put('/pagamentosEdit', pagamentoController.edit);
routes.get('/pagamentosPDV', pagamentoController.pdvselect);

const empresaController = require('./controller/EmpresaController');
routes.get('/empresasGetEdit', empresaController.getedit);
routes.put('/empresasEdit', empresaController.edit);

const pdvController = require('./controller/PdvController');
routes.post('/pdvCreate', pdvController.inserir);
routes.get('/pdvStatusCaixa', pdvController.statusCaixa);
routes.post('/pdvAbrirCaixa', pdvController.abrirCaixa);

module.exports = routes;